package org;

import java.io.IOException;
import java.awt.image.RenderedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.Color;
import java.awt.image.BufferedImage;

public class ColorPicker
{
    public static void main(final String[] args) {
        System.out.println(System.currentTimeMillis());
        final int rad = 1024;
        final BufferedImage img = new BufferedImage(rad, rad, 1);
        final int centerX = img.getWidth() / 2;
        final int centerY = img.getHeight() / 2;
        final int radius = img.getWidth() / 2 * (img.getWidth() / 2);
        final int redX = img.getWidth();
        final int redY = img.getHeight() / 2;
        final int redRad = img.getWidth() * img.getWidth();
        final int greenX = 0;
        final int greenY = img.getHeight() / 2;
        final int greenRad = img.getWidth() * img.getWidth();
        final int blueX = img.getWidth() / 2;
        final int blueY = img.getHeight();
        final int blueRad = img.getWidth() * img.getWidth();
        for (int i = 0; i < img.getWidth(); ++i) {
            for (int j = 0; j < img.getHeight(); ++j) {
                final int a = i - centerX;
                final int b = j - centerY;
                final int distance = a * a + b * b;
                if (distance < radius) {
                    final int rdx = i - redX;
                    final int rdy = j - redY;
                    final int redDist = rdx * rdx + rdy * rdy;
                    final int redVal = (int)(255.0f - redDist / redRad * 256.0f);
                    final int gdx = i - greenX;
                    final int gdy = j - greenY;
                    final int greenDist = gdx * gdx + gdy * gdy;
                    final int greenVal = (int)(255.0f - greenDist / greenRad * 256.0f);
                    final int bdx = i - blueX;
                    final int bdy = j - blueY;
                    final int blueDist = bdx * bdx + bdy * bdy;
                    final int blueVal = (int)(255.0f - blueDist / blueRad * 256.0f);
                    final Color c = new Color(redVal, greenVal, blueVal);
                    final float[] hsbVals = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);
                    final Color highlight = Color.getHSBColor(hsbVals[0], hsbVals[1], 1.0f);
                    img.setRGB(i, j, RGBtoHEX(highlight));
                }
                else {
                    img.setRGB(i, j, -1);
                }
            }
        }
        System.out.println(System.currentTimeMillis());
        try {
            ImageIO.write(img, "png", new File("wheel.png"));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static int RGBtoHEX(final Color color) {
        String hex = Integer.toHexString(color.getRGB() & 0xFFFFFF);
        if (hex.length() < 6) {
            if (hex.length() == 5) {
                hex = "0" + hex;
            }
            if (hex.length() == 4) {
                hex = "00" + hex;
            }
            if (hex.length() == 3) {
                hex = "000" + hex;
            }
        }
        hex = "#" + hex;
        return Integer.decode(hex);
    }
}
