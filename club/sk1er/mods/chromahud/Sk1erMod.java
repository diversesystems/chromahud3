package club.sk1er.mods.chromahud;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Sk1erMod
{
    private static Sk1erMod instance;
    private boolean first;
    private List<ITextComponent> updateMessage;
    private String modid;
    private String version;
    private boolean enabled;
    private boolean hasUpdate;
    private String name;
    private String apiKey;
    private String prefix;
    private JsonHolder en;
    private boolean hypixel;
    private GenKeyCallback callback;
    private ConcurrentLinkedQueue<ITextComponent> messages;
    private boolean bookUser;
    private boolean firstFileStatus;
    private File dir;
    private boolean book;
    
    public Sk1erMod(final String modid, final String version, final String name) {
        this.first = false;
        this.updateMessage = new ArrayList<ITextComponent>();
        this.enabled = true;
        this.hasUpdate = false;
        this.messages = new ConcurrentLinkedQueue<ITextComponent>();
        this.bookUser = false;
        this.firstFileStatus = false;
        this.book = false;
        this.modid = modid;
        this.version = version;
        this.name = name;
        Sk1erMod.instance = this;
        this.prefix = TextFormatting.RED + "[" + TextFormatting.AQUA + this.name + TextFormatting.RED + "]" + TextFormatting.YELLOW + ": ";
        MinecraftForge.EVENT_BUS.register((Object)this);
        final File mcDataDir = Minecraft.getMinecraft().mcDataDir;
        this.dir = new File(mcDataDir, "sk1ermod");
        if (!this.dir.exists()) {
            this.dir.mkdirs();
        }
    }
    
    public Sk1erMod(final String modid, final String version, final String name, final GenKeyCallback callback) {
        this(modid, version, name);
        this.callback = callback;
    }
    
    public static Sk1erMod getInstance() {
        return Sk1erMod.instance;
    }
    
    public boolean hasUpdate() {
        return this.hasUpdate;
    }
    
    public boolean isEnabled() {
        return false;
    }
    
    public List<ITextComponent> getUpdateMessage() {
        return this.updateMessage;
    }

//    public void sendMessage(final String message) {
//        this.messages.add((ITextComponent)new TextComponentString(this.prefix + message));
//    }
//
//    @SubscribeEvent
//    public void tick(final TickEvent.RenderTickEvent event) {
//        if (Minecraft.getMinecraft().player == null) {
//            return;
//        }
//        while (!this.messages.isEmpty()) {
//            Minecraft.getMinecraft().player.sendChatMessage((ITextComponent)this.messages.poll());
//        }
//
//    }

    
    private void process(final String input) {
        this.updateMessage.add(ForgeHooks.newChatWithLinks(input));
    }
    
    private void checkFirst(final String lock, final boolean first) {
        if (lock.isEmpty()) {
            return;
        }
        final File tmp = new File(this.dir, lock);
        if (!tmp.exists()) {
            try {
                tmp.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            this.firstFileStatus = first;
        }
    }

    
    private void sendMessage(final ITextComponent s) {
        this.messages.add(s);
    }

    
    @SubscribeEvent
    public void onPlayerLogOutEvent(final FMLNetworkEvent.ClientDisconnectionFromServerEvent event) {
        this.hypixel = false;
    }

}
