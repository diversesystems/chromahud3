package club.sk1er.mods.chromahud.commands;

import club.sk1er.mods.chromahud.ChromaHUD;
import club.sk1er.mods.chromahud.ElementRenderer;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

public class CommandChromaHud extends CommandBase
{
    private ChromaHUD mod;
    
    public CommandChromaHud(final ChromaHUD mod) {
        this.mod = mod;
    }

    public boolean checkPermission(MinecraftServer server, ICommandSender sender)
    {
        return true;
    }
    
    public String getName() {
        return "chromahud";
    }
    
    public String getUsage(final ICommandSender sender) {
        return "/chromahud";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            this.mod.setup();
        }
        else {
            ElementRenderer.display();
        }
    }

}
