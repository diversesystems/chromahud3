package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeHud extends DisplayItem
{
    private String format;
    
    public TimeHud(final JsonHolder data, final int ordinal) {
        super(data, ordinal);
        this.format = data.optString("format");
        if (this.format.isEmpty()) {
            this.format = "HH:mm:ss";
        }
    }
    
    public String getFormat() {
        return this.format;
    }
    
    public void setFormat(final String format) {
        this.data.put("format", format);
        this.format = format;
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean isConfig) {
        try {
            final String string = new SimpleDateFormat(this.format).format(new Date(System.currentTimeMillis()));
            ElementRenderer.draw(starX, startY, string);
            return new Dimension(isConfig ? ((double)Minecraft.getMinecraft().fontRenderer.getStringWidth(string)) : 0.0, 10.0);
        }
        catch (Exception e) {
            ElementRenderer.draw(starX, startY, "Invalid");
            return new Dimension(0.0, 0.0);
        }
    }
}
