package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;

public class CCounter extends DisplayItem
{
    public CCounter(final JsonHolder raw, final int ordinal) {
        super(raw, ordinal);
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean ignored) {
        final String string = ElementRenderer.getCValue();
        ElementRenderer.draw(starX, startY, string);
        return new Dimension(Minecraft.getMinecraft().fontRenderer.getStringWidth(string), 10.0);
    }
}
