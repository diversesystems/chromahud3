package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ArrowCount extends DisplayItem
{
    private JsonHolder data;
    
    public ArrowCount(final JsonHolder data, final int ordinal) {
        super(data, ordinal);
        this.data = data;
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean isConfig) {
        final List<ItemStack> list = new ArrayList<ItemStack>();
        list.add(new ItemStack(Item.getItemById(262), 64));
        final EntityPlayerSP thePlayer = Minecraft.getMinecraft().player;
        if (thePlayer != null) {
            int c = 0;
            for (final ItemStack is : thePlayer.inventory.mainInventory) {
                if (is != null && is.getUnlocalizedName().equalsIgnoreCase("item.arrow")) {
                    c += is.getCount();
                }
            }
            ElementRenderer.render(list, starX, startY, false);
            ElementRenderer.draw(starX + 16, startY + 8.0, "x" + (isConfig ? 64 : c));
            return new Dimension(16.0, 16.0);
        }
        return new Dimension(0.0, 0.0);
    }
}
