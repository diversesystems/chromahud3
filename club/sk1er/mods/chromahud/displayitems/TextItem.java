package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;

import java.util.ArrayList;
import java.util.List;

public class TextItem extends DisplayItem
{
    private String text;
    
    public TextItem(final JsonHolder object, final int ordinal) {
        super(object, ordinal);
        this.text = object.optString("text");
    }
    
    @Override
    public Dimension draw(final int x, final double y, final boolean isConfig) {
        final List<String> list = new ArrayList<String>();
        if (this.text.isEmpty()) {
            list.add("Text is empty??");
        }
        else {
            list.add(this.text);
        }
        ElementRenderer.draw(x, y, list);
        return new Dimension(ElementRenderer.maxWidth(list), 10.0);
    }
    
    public String getText() {
        return this.text;
    }
    
    public void setText(final String text) {
        this.text = text;
        this.data.put("text", text);
    }
}
