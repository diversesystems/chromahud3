package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

import java.util.ArrayList;
import java.util.List;

public class ArmourHud extends DisplayItem
{
    List<ItemStack> list;
    private int ordinal;
    private boolean dur;
    private boolean hand;
    
    public ArmourHud(final JsonHolder raw, final int ordinal) {
        super(raw, ordinal);
        this.list = new ArrayList<ItemStack>();
        this.dur = false;
        this.hand = false;
        this.dur = raw.optBoolean("dur");
        this.hand = raw.optBoolean("hand");
    }
    
    @Override
    public void save() {
        this.data.put("dur", this.dur);
        this.data.put("hand", this.hand);
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean isConfig) {
        this.list.clear();
        if (isConfig) {
            this.list.add(new ItemStack(Item.getItemById(276), 1));
            this.list.add(new ItemStack(Item.getItemById(261), 1));
            this.list.add(new ItemStack(Item.getItemById(262), 64));
            this.list.add(new ItemStack(Item.getItemById(310), 1));
            this.list.add(new ItemStack(Item.getItemById(311), 1));
            this.list.add(new ItemStack(Item.getItemById(312), 1));
            this.list.add(new ItemStack(Item.getItemById(313), 1));
        }
        else {
            this.list = this.itemsToRender();
        }
        this.drawArmour(starX, startY);
        return new Dimension(16.0, this.getHeight());
    }
    
    public void drawArmour(final int x, final double y) {
        final RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
        final EntityPlayerSP thePlayer = Minecraft.getMinecraft().player;
        if (thePlayer == null || renderItem == null) {
            return;
        }
        ElementRenderer.render(this.list, x, y, this.dur);
    }
    
    public int getHeight() {
        return this.list.size() * 16;
    }
    
    private List<ItemStack> itemsToRender() {
        final List<ItemStack> items = new ArrayList<ItemStack>();
        final ItemStack heldItem = Minecraft.getMinecraft().player.getHeldItemMainhand();
        if (this.hand && heldItem != null) {
            items.add(heldItem);
        }
        final NonNullList<ItemStack> inventory = Minecraft.getMinecraft().player.inventory.armorInventory;
        for (int i = 3; i >= 0; --i) {
            if (inventory.get(i) != null && inventory.get(i).getItem() != null) {
                items.add(inventory.get(i));
            }
        }
        for (final ItemStack is : Minecraft.getMinecraft().player.inventory.mainInventory) {
            if (is != null && is.getUnlocalizedName().equalsIgnoreCase("item.bow")) {
                items.add(is);
                break;
            }
        }
        return items;
    }
    
    public void toggleDurability() {
        this.dur = !this.dur;
    }
    
    public void toggleHand() {
        this.hand = !this.hand;
    }
}
