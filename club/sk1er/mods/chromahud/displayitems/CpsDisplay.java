package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;

public class CpsDisplay extends DisplayItem
{
    public CpsDisplay(final JsonHolder data, final int ordinal) {
        super(data, ordinal);
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean isConfig) {
        ElementRenderer.draw(starX, startY, "CPS: " + ElementRenderer.getCPS());
        if (isConfig) {
            return new Dimension(Minecraft.getMinecraft().fontRenderer.getStringWidth("CPS: " + ElementRenderer.getCPS()), 10.0);
        }
        return new Dimension(0.0, 10.0);
    }
}
