package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;

public class PingDisplay extends DisplayItem
{
    public PingDisplay(final JsonHolder raw, final int ordinal) {
        super(raw, ordinal);
    }
    
    @Override
    public Dimension draw(final int starX, final double startY, final boolean isConfig) {
        final EntityPlayerSP thePlayer = Minecraft.getMinecraft().player;
        if (thePlayer != null) {
            final String string = "Ping: " + Minecraft.getMinecraft().getConnection().getPlayerInfo(Minecraft.getMinecraft().player.getUniqueID()).getResponseTime();
            ElementRenderer.draw(starX, startY, string);
            return new Dimension(Minecraft.getMinecraft().fontRenderer.getStringWidth(string), 10.0);
        }
        return new Dimension(0.0, 0.0);
    }
}
