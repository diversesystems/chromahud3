package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PotionEffects extends DisplayItem
{
    public PotionEffects(final JsonHolder raw, final int ordinal) {
        super(raw, ordinal);
    }
    
    @Override
    public Dimension draw(final int x, final double y, final boolean isConfig) {
        int row = 0;
        final double scale = ElementRenderer.getCurrentScale();
        Collection<PotionEffect> effects = new ArrayList<PotionEffect>();
        if (isConfig) {
            effects.add(new PotionEffect(Potion.getPotionById(1), 100, 1));
            effects.add(new PotionEffect(Potion.getPotionById(3), 100, 2));
        }
        else {
            effects = (Collection<PotionEffect>)Minecraft.getMinecraft().player.getActivePotionEffects();
        }
        final List<String> tmp = new ArrayList<String>();
        for (final PotionEffect potioneffect : effects) {
            final Potion potion = potioneffect.getPotion();
            if (!potion.shouldRender(potioneffect)) {
                continue;
            }
            potion.renderInventoryEffect(x, (int)(y + row * 24 * scale / 100.0), potioneffect, Minecraft.getMinecraft());
            final StringBuilder s1 = new StringBuilder(I18n.format(potion.getName(), new Object[0]));
            if (potioneffect.getAmplifier() == 1) {
                s1.append(" ").append(I18n.format("enchantment.level.2", new Object[0]));
            }
            else if (potioneffect.getAmplifier() == 2) {
                s1.append(" ").append(I18n.format("enchantment.level.3", new Object[0]));
            }
            else if (potioneffect.getAmplifier() == 3) {
                s1.append(" ").append(I18n.format("enchantment.level.4", new Object[0]));
            }
            final String s2 = Potion.getPotionDurationString(potioneffect,1.0f);
            final String text = (Object)s1 + " - " + s2;
            tmp.add(text);
            ElementRenderer.draw((int)(x / scale), y + (double)(row * 16), text);
            ++row;
        }
        return new Dimension(isConfig ? ElementRenderer.maxWidth(tmp) : 0.0, row * 16);
    }
}
