package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;
import java.util.List;

public class DirectionHUD extends DisplayItem
{
    private static final String[] dir;
    
    public DirectionHUD(final JsonHolder raw, final int ordinal) {
        super(raw, ordinal);
    }
    
    @Override
    public Dimension draw(final int x, final double y, final boolean isConfig) {
        final List<String> list = new ArrayList<String>();
        final EntityPlayer player = (EntityPlayer)Minecraft.getMinecraft().player;
        if (player != null) {
            int d = (int)player.rotationYaw;
            if (d < 0) {
                d += 360;
            }
            d += 22;
            int direction = d % 360;
            direction /= 45;
            try {
                while (direction < 0) {
                    direction += 8;
                }
                list.add(DirectionHUD.dir[direction]);
            }
            catch (Exception ex) {}
        }
        ElementRenderer.draw(x, y, list);
        return new Dimension(isConfig ? ((double)ElementRenderer.maxWidth(list)) : 0.0, 10.0);
    }
    
    static {
        dir = new String[] { "South", "South West", "West", "North West", "North", "North East", "East", "South East" };
    }
}
