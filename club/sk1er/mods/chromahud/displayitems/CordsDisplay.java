package club.sk1er.mods.chromahud.displayitems;

import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.JsonHolder;
import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import com.google.gson.JsonObject;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CordsDisplay extends DisplayItem
{
    public int state;
    public int precision;
    private JsonObject raw;
    
    public CordsDisplay(final JsonHolder options, final int orderinal) {
        super(options, orderinal);
        this.state = 0;
        this.precision = 1;
        this.state = options.optInt("state");
        this.precision = options.optInt("precision");
    }
    
    @Override
    public void save() {
        this.data.put("state", this.state);
        this.data.put("precision", this.precision);
    }
    
    @Override
    public String toString() {
        return "CordsDisplay{state=" + this.state + '}';
    }
    
    @Override
    public Dimension draw(final int x, final double y, final boolean isConfig) {
        final List<String> tmp = new ArrayList<String>();
        final EntityPlayer player = (EntityPlayer)Minecraft.getMinecraft().player;
        if (player != null) {
            final StringBuilder start = new StringBuilder("0");
            if (this.precision > 0) {
                start.append(".");
            }
            for (int i = 0; i < this.precision; ++i) {
                start.append("0");
            }
            final DecimalFormat df = new DecimalFormat(start.toString());
            if (this.state == 0) {
                tmp.add("X: " + df.format(player.posX) + " Y: " + df.format(player.posY) + " Z: " + df.format(player.posZ));
            }
            else if (this.state == 1) {
                tmp.add("X: " + df.format(player.posX));
                tmp.add("Y: " + df.format(player.posY));
                tmp.add("Z: " + df.format(player.posZ));
            }
            else {
                tmp.add("Illegal state of cords unit (" + this.state + ")");
            }
        }
        else {
            tmp.add("X: null, Y: null, Z: null");
        }
        ElementRenderer.draw(x, y, tmp);
        return new Dimension(isConfig ? ElementRenderer.maxWidth(tmp) : 0.0, tmp.size() * 10);
    }
}
