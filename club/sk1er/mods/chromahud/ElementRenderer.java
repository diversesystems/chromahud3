package club.sk1er.mods.chromahud;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ElementRenderer
{
    protected static ScaledResolution resolution;
    private static double currentScale;
    private static int color;
    private static int[] COLORS;
    private static boolean display;
    private static List<Long> clicks;
    private static DisplayElement current;
    private static FontRenderer fontRendererObj;
    private static String cValue;
    boolean last;
    private ChromaHUD mod;
    private Minecraft minecraft;
    
    public ElementRenderer(final ChromaHUD mod) {
        this.last = false;
        this.mod = mod;
        this.minecraft = Minecraft.getMinecraft();
    }
    
    public static String getCValue() {
        return ElementRenderer.cValue;
    }
    
    public static double getCurrentScale() {
        return ElementRenderer.currentScale;
    }
    
    public static int getColor(final int c, final int x) {
        return c;
    }
    
    public static void display() {
        ElementRenderer.display = true;
    }
    
    public static void draw(final int x, final double y, final String string) {
        final List<String> tmp = new ArrayList<String>();
        tmp.add(string);
        draw(x, y, tmp);
    }
    
    public static int RGBtoHEX(final Color color) {
        String hex = Integer.toHexString(color.getRGB() & 0xFFFFFF);
        if (hex.length() < 6) {
            if (hex.length() == 5) {
                hex = "0" + hex;
            }
            if (hex.length() == 4) {
                hex = "00" + hex;
            }
            if (hex.length() == 3) {
                hex = "000" + hex;
            }
        }
        hex = "#" + hex;
        return Integer.decode(hex);
    }
    
    public static void draw(final int x, final double y, final List<String> list) {
        double ty = y;
        for (final String string : list) {
            if (ElementRenderer.current.isHighlighted()) {
                final int stringWidth = ElementRenderer.fontRendererObj.getStringWidth(string);
                Gui.drawRect((int)((x - 1) / getCurrentScale()), (int)((ty - 1.0) / getCurrentScale()), (int)((x + 1) / getCurrentScale()) + stringWidth, (int)((ty + 1.0) / getCurrentScale()) + 8, new Color(0, 0, 0, 125).getRGB());
            }
            if (ElementRenderer.current.isChroma()) {
                drawChromaString(string, x, (int)ty);
            }
            else {
                ElementRenderer.fontRendererObj.drawString(string, (float)(int)(x / getCurrentScale()), (float)(int)(ty / getCurrentScale()), getColor(ElementRenderer.color, x), ElementRenderer.current.isShadow());
            }
            ty += 10.0;
        }
    }
    
    public static void drawChromaString(final String text, final int xIn, final int y) {
        final FontRenderer renderer = Minecraft.getMinecraft().fontRenderer;
        int x = xIn;
        for (final char c : text.toCharArray()) {
            long dif = (long)(x * 10 - y * 10);
            if (ElementRenderer.current.isStaticChroma()) {
                dif = 0L;
            }
            final long l = System.currentTimeMillis() - dif;
            final float ff = ElementRenderer.current.isStaticChroma() ? 1000.0f : 2000.0f;
            final int i = Color.HSBtoRGB((float)(l % (int)ff) / ff, 0.8f, 0.8f);
            final String tmp = String.valueOf(c);
            renderer.drawString(tmp, (float)(x / getCurrentScale()), (float)(y / getCurrentScale()), i, ElementRenderer.current.isShadow());
            x += renderer.getCharWidth(c) * getCurrentScale();
        }
    }
    
    private static boolean isChromaInt(final int e) {
        return e >= 0 && e <= 1;
    }
    
    public static int maxWidth(final List<String> list) {
        int max = 0;
        for (final String s : list) {
            max = Math.max(max, Minecraft.getMinecraft().fontRenderer.getStringWidth(s));
        }
        return max;
    }
    
    public static int getColor() {
        return ElementRenderer.color;
    }
    
    public static int getCPS() {
        final Iterator<Long> iterator = ElementRenderer.clicks.iterator();
        while (iterator.hasNext()) {
            if (System.currentTimeMillis() - iterator.next() > 1000L) {
                iterator.remove();
            }
        }
        return ElementRenderer.clicks.size();
    }
    
    public static DisplayElement getCurrent() {
        return ElementRenderer.current;
    }
    
    public static void render(final List<ItemStack> itemStacks, final int x, final double y, final boolean showDurability) {
        int line = 0;
        final RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
        for (final ItemStack stack : itemStacks) {
            renderItem.renderItemAndEffectIntoGUI(stack, (int)(x / getCurrentScale()), (int)((y + 16 * line * getCurrentScale()) / getCurrentScale()));
            if (showDurability) {
                final String dur = stack.getMaxDamage() - stack.getItemDamage() + "/" + stack.getMaxDamage();
                draw((int)(x + 20.0 * ElementRenderer.currentScale), y + 16 * line + 8.0, dur);
            }
            ++line;
        }
    }
    
    public static void startDrawing(final DisplayElement element) {
        GlStateManager.scale(element.getScale(), element.getScale(), 0.0);
        ElementRenderer.currentScale = element.getScale();
        ElementRenderer.color = element.getColor();
        ElementRenderer.current = element;
    }
    
    public static void endDrawing(final DisplayElement element) {
        GlStateManager.scale(1.0 / element.getScale(), 1.0 / element.getScale(), 0.0);
    }
    
    @SubscribeEvent
    public void tick(final TickEvent.ClientTickEvent event) {
        if (ElementRenderer.display) {
            Minecraft.getMinecraft().displayGuiScreen((GuiScreen)this.mod.getConfigGuiInstance());
            ElementRenderer.display = false;
        }
        if (Minecraft.getMinecraft().inGameHasFocus) {
            ElementRenderer.cValue = Minecraft.getMinecraft().renderGlobal.getDebugInfoRenders().split("/")[0].trim();
        }
    }
    
    @SubscribeEvent
    public void onRenderTick(final TickEvent.RenderTickEvent event) {
        ElementRenderer.resolution = new ScaledResolution(Minecraft.getMinecraft());
        if (!this.minecraft.inGameHasFocus || this.minecraft.gameSettings.showDebugInfo) {
            return;
        }
        this.renderElements();
    }
    
    public void renderElements() {
        if (ElementRenderer.fontRendererObj == null) {
            ElementRenderer.fontRendererObj = Minecraft.getMinecraft().fontRenderer;
        }
        final boolean m = Mouse.isButtonDown(0);
        if (m != this.last && (this.last = m)) {
            ElementRenderer.clicks.add(System.currentTimeMillis());
        }
        final List<DisplayElement> elementList = this.mod.getDisplayElements();
        for (final DisplayElement element : elementList) {
            startDrawing(element);
            try {
                element.draw();
            }
            catch (Exception ex) {}
            endDrawing(element);
        }
    }
    
    static {
        ElementRenderer.currentScale = 1.0;
        ElementRenderer.COLORS = new int[] { 16777215, 16711680, 65280, 255, 16776960, 11141290 };
        ElementRenderer.display = false;
        ElementRenderer.clicks = new ArrayList<Long>();
        ElementRenderer.fontRendererObj = Minecraft.getMinecraft().fontRenderer;
    }
}
