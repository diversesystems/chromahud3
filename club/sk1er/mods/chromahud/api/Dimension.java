package club.sk1er.mods.chromahud.api;

public class Dimension
{
    private double width;
    private double height;
    
    public Dimension(final double width, final double height) {
        this.width = width;
        this.height = height;
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public double getHeight() {
        return this.height;
    }
}
