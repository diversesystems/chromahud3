package club.sk1er.mods.chromahud.api;

public class ChromaHUDDescription
{
    private String id;
    private String version;
    private String name;
    private String description;
    
    public ChromaHUDDescription(final String id, final String version, final String name, final String description) {
        this.id = id;
        this.version = version;
        this.name = name;
        this.description = description;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getVersion() {
        return this.version;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getDescription() {
        return this.description;
    }
}
