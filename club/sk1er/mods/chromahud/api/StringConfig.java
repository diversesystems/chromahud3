package club.sk1er.mods.chromahud.api;

import java.util.function.Consumer;

public class StringConfig
{
    private String string;
    private Consumer<DisplayItem> load;
    private Consumer<DisplayItem> draw;
    
    public StringConfig(final String string, final Consumer<DisplayItem> load, final Consumer<DisplayItem> draw) {
        this.string = string;
        this.load = load;
        this.draw = draw;
    }
    
    public StringConfig(final String string) {
        this.string = string;
        this.load = (displayItem -> {});
        this.draw = (displayItem -> {});
    }
    
    public Consumer<DisplayItem> getLoad() {
        return this.load;
    }
    
    public Consumer<DisplayItem> getDraw() {
        return this.draw;
    }
    
    public String getString() {
        return this.string;
    }
    
    public void setString(final String string) {
        this.string = string;
    }
}
