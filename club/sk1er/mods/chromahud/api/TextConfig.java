package club.sk1er.mods.chromahud.api;

import net.minecraft.client.gui.GuiTextField;

import java.util.function.BiConsumer;

public class TextConfig
{
    private BiConsumer<GuiTextField, DisplayItem> action;
    private GuiTextField button;
    private BiConsumer<GuiTextField, DisplayItem> load;
    
    public TextConfig(final BiConsumer<GuiTextField, DisplayItem> action, final GuiTextField button, final BiConsumer<GuiTextField, DisplayItem> load) {
        this.action = action;
        this.button = button;
        this.load = load;
    }
    
    public BiConsumer<GuiTextField, DisplayItem> getAction() {
        return this.action;
    }
    
    public GuiTextField getTextField() {
        return this.button;
    }
    
    public BiConsumer<GuiTextField, DisplayItem> getLoad() {
        return this.load;
    }
}
