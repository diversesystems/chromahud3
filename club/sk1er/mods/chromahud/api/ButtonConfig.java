package club.sk1er.mods.chromahud.api;

import net.minecraft.client.gui.GuiButton;

import java.util.function.BiConsumer;

public class ButtonConfig
{
    private BiConsumer<GuiButton, DisplayItem> action;
    private GuiButton button;
    private BiConsumer<GuiButton, DisplayItem> load;
    
    public ButtonConfig(final BiConsumer<GuiButton, DisplayItem> action, final GuiButton button, final BiConsumer<GuiButton, DisplayItem> load) {
        this.action = action;
        this.button = button;
        this.load = load;
    }
    
    public BiConsumer<GuiButton, DisplayItem> getAction() {
        return this.action;
    }
    
    public GuiButton getButton() {
        return this.button;
    }
    
    public BiConsumer<GuiButton, DisplayItem> getLoad() {
        return this.load;
    }
}
