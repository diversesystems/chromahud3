package club.sk1er.mods.chromahud.api;

import club.sk1er.mods.chromahud.ChromaHUDApi;
import club.sk1er.mods.chromahud.JsonHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class DisplayItem
{
    private int ordinal;
    protected JsonHolder data;
    
    public JsonHolder getData() {
        this.save();
        return this.data;
    }
    
    public void save() {
    }
    
    public DisplayItem(final JsonHolder data, final int ordinal) {
        this.data = data;
        this.ordinal = ordinal;
    }
    
    public String name() {
        return ChromaHUDApi.getInstance().getName(this.data.optString("type"));
    }
    
    public int getOrdinal() {
        return this.ordinal;
    }
    
    public void setOrdinal(final int ordinal) {
        this.ordinal = ordinal;
    }
    
    public abstract Dimension draw(final int p0, final double p1, final boolean p2);
    
    public List<ButtonConfig> configOptions() {
        return new ArrayList<ButtonConfig>();
    }
    
    public String getType() {
        return this.data.optString("type");
    }
}
