package club.sk1er.mods.chromahud.api;

import club.sk1er.mods.chromahud.JsonHolder;

import java.util.Map;

public interface ChromaHUDParser
{
    DisplayItem parse(final String p0, final int p1, final JsonHolder p2);
    
    Map<String, String> getNames();
    
    ChromaHUDDescription description();
}
