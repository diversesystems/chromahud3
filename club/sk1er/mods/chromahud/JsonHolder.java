package club.sk1er.mods.chromahud;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JsonHolder
{
    private JsonObject object;
    
    public JsonHolder(final JsonObject object) {
        this.object = object;
    }
    
    public JsonHolder(final String raw) {
        this(new JsonParser().parse(raw).getAsJsonObject());
    }
    
    public JsonHolder() {
        this(new JsonObject());
    }
    
    public static String[] getNames(final JsonHolder statsObject) {
        final List<String> keys = statsObject.getKeys();
        final String[] keyArray = new String[keys.size()];
        for (int i = 0; i < keys.size(); ++i) {
            keyArray[i] = keys.get(i);
        }
        return keyArray;
    }
    
    @Override
    public String toString() {
        return this.object.toString();
    }
    
    public JsonHolder put(final String key, final boolean value) {
        this.object.addProperty(key, value);
        return this;
    }
    
    public void mergeNotOverride(final JsonHolder merge) {
        this.merge(merge, false);
    }
    
    public void mergeOverride(final JsonHolder merge) {
        this.merge(merge, true);
    }
    
    public void merge(final JsonHolder merge, final boolean override) {
        final JsonObject object = merge.getObject();
        for (final String s : merge.getKeys()) {
            if (override || !this.has(s)) {
                this.put(s, object.get(s));
            }
        }
    }
    
    public JsonHolder put(final String key, final String value) {
        this.object.addProperty(key, value);
        return this;
    }
    
    public JsonHolder put(final String key, final int value) {
        this.object.addProperty(key, (Number)value);
        return this;
    }
    
    public JsonHolder put(final String key, final double value) {
        this.object.addProperty(key, (Number)value);
        return this;
    }
    
    public JsonHolder put(final String key, final long value) {
        this.object.addProperty(key, (Number)value);
        return this;
    }
    
    public JsonHolder optJsonObject(final String key, final JsonObject fallBack) {
        try {
            return new JsonHolder(this.object.get(key).getAsJsonObject());
        }
        catch (Exception e) {
            return new JsonHolder(fallBack);
        }
    }
    
    public JsonHolder optJsonObject(final String key, final JsonHolder fallBack) {
        try {
            return new JsonHolder(this.object.get(key).getAsJsonObject());
        }
        catch (Exception e) {
            return fallBack;
        }
    }
    
    public JsonArray optJSONArray(final String key, final JsonArray fallback) {
        try {
            return this.object.get(key).getAsJsonArray();
        }
        catch (Exception e) {
            return fallback;
        }
    }
    
    public JsonArray optJSONArray(final String key) {
        return this.optJSONArray(key, new JsonArray());
    }
    
    public boolean has(final String key) {
        return this.object.has(key);
    }
    
    public long optLong(final String key, final long fallback) {
        try {
            return this.object.get(key).getAsLong();
        }
        catch (Exception e) {
            return fallback;
        }
    }
    
    public long optLong(final String key) {
        return this.optLong(key, 0L);
    }
    
    public boolean optBoolean(final String key, final boolean fallback) {
        try {
            return this.object.get(key).getAsBoolean();
        }
        catch (Exception e) {
            return fallback;
        }
    }
    
    public boolean optBoolean(final String key) {
        return this.optBoolean(key, false);
    }
    
    public JsonObject optActualJsonObject(final String key) {
        try {
            return this.object.get(key).getAsJsonObject();
        }
        catch (Exception e) {
            return new JsonObject();
        }
    }
    
    public JsonHolder optJsonObject(final String key) {
        return this.optJsonObject(key, new JsonObject());
    }
    
    public int optInt(final String key, final int fallBack) {
        try {
            return this.object.get(key).getAsInt();
        }
        catch (Exception e) {
            return fallBack;
        }
    }
    
    public int optInt(final String key) {
        return this.optInt(key, 0);
    }
    
    public String optString(final String key, final String fallBack) {
        try {
            return this.object.get(key).getAsString();
        }
        catch (Exception e) {
            return fallBack;
        }
    }
    
    public String optString(final String key) {
        return this.optString(key, "");
    }
    
    public double optDouble(final String key, final double fallBack) {
        try {
            return this.object.get(key).getAsDouble();
        }
        catch (Exception e) {
            return fallBack;
        }
    }
    
    public List<String> getKeys() {
        final List<String> tmp = new ArrayList<String>();
        for (final Map.Entry<String, JsonElement> stringJsonElementEntry : this.object.entrySet()) {
            tmp.add(stringJsonElementEntry.getKey());
        }
        return tmp;
    }
    
    public double optDouble(final String key) {
        return this.optDouble(key, 0.0);
    }
    
    public JsonObject getObject() {
        return this.object;
    }
    
    public boolean isNull(final String key) {
        return this.object.has(key) && this.object.get(key).isJsonNull();
    }
    
    public JsonHolder put(final String values, final JsonHolder values1) {
        this.put(values, values1.getObject());
        return this;
    }
    
    public void put(final String values, final JsonObject object) {
        this.object.add(values, (JsonElement)object);
    }
    
    public void remove(final String header) {
        this.object.remove(header);
    }
    
    public String getString(final String seed) {
        return this.optString(seed);
    }
    
    public JsonArray optJSONArrayNonNull(final String matches) {
        return this.optJSONArray(matches);
    }
    
    public int getInt(final String type) {
        return this.optInt(type);
    }
    
    public UUID getUUID(final String offender) {
        return this.optUUID(offender);
    }
    
    private UUID optUUID(final String offender) {
        return UUID.fromString(this.optString(offender));
    }
    
    public String getOptString(final String removedBy) {
        return this.optString(removedBy, null);
    }
    
    public long getLong(final String dateIssued) {
        return this.optLong(dateIssued);
    }
    
    public long getOptLong(final String removeDate, final long l) {
        return this.optLong(removeDate, l);
    }
    
    public Object get(final String field) {
        return this.object.get(field);
    }
    
    public void put(final String key, final JsonArray jsonArrayHolder) {
        this.object.add(key, (JsonElement)jsonArrayHolder);
    }
    
    public JsonHolder put(final String key, final JsonElement element) {
        this.object.add(key, element);
        return this;
    }
    
    public JsonHolder put(final String key, final Object value) {
        return this.put(key, value.toString());
    }
    
    public JsonHolder put(final String key, final Number value) {
        this.object.add(key, (JsonElement)new JsonPrimitive(value));
        return this;
    }
    
    public JsonHolder getJsonHolder(final String entry) {
        return this.optJsonObject(entry);
    }
    
    public void putNull(final String key) {
        this.object.add(key, (JsonElement)JsonNull.INSTANCE);
    }
    
    public JsonElement getElement(final String field) {
        return this.object.get(field);
    }
}
