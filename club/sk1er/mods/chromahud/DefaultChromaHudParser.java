package club.sk1er.mods.chromahud;

import club.sk1er.mods.chromahud.api.ChromaHUDDescription;
import club.sk1er.mods.chromahud.api.ChromaHUDParser;
import club.sk1er.mods.chromahud.api.DisplayItem;
import club.sk1er.mods.chromahud.displayitems.*;

import java.util.HashMap;
import java.util.Map;

public class DefaultChromaHudParser implements ChromaHUDParser
{
    private HashMap<String, String> names;
    
    public DefaultChromaHudParser() {
        (this.names = new HashMap<String, String>()).put("CORDS", "Cords");
        this.names.put("TEXT", "Text");
        this.names.put("ARMOUR_HUD", "Armour");
        this.names.put("POTION", "Potion");
        this.names.put("FPS", "FPS");
        this.names.put("PING", "Ping");
        this.names.put("DIRECTION", "Direction");
        this.names.put("CPS", "CPS");
        this.names.put("ARROW_COUNT", "Arrow Count");
        this.names.put("TIME", "Time");
        this.names.put("C_COUNTER", "C Counter");
    }
    
    @Override
    public DisplayItem parse(final String type, final int ord, final JsonHolder item) {
        int n = -1;
        switch (type.hashCode()) {
            case 64310389: {
                if (type.equals("CORDS")) {
                    n = 0;
                    break;
                }
                break;
            }
            case 2571565: {
                if (type.equals("TEXT")) {
                    n = 1;
                    break;
                }
                break;
            }
            case -1407988856: {
                if (type.equals("ARMOUR_HUD")) {
                    n = 2;
                    break;
                }
                break;
            }
            case -1929101933: {
                if (type.equals("POTION")) {
                    n = 3;
                    break;
                }
                break;
            }
            case 69833: {
                if (type.equals("FPS")) {
                    n = 4;
                    break;
                }
                break;
            }
            case 2455922: {
                if (type.equals("PING")) {
                    n = 5;
                    break;
                }
                break;
            }
            case 1824003935: {
                if (type.equals("DIRECTION")) {
                    n = 6;
                    break;
                }
                break;
            }
            case 66950: {
                if (type.equals("CPS")) {
                    n = 7;
                    break;
                }
                break;
            }
            case 976217369: {
                if (type.equals("ARROW_COUNT")) {
                    n = 8;
                    break;
                }
                break;
            }
            case -1101294112: {
                if (type.equals("C_COUNTER")) {
                    n = 9;
                    break;
                }
                break;
            }
            case 2575053: {
                if (type.equals("TIME")) {
                    n = 10;
                    break;
                }
                break;
            }
        }
        switch (n) {
            case 0: {
                return new CordsDisplay(item, ord);
            }
            case 1: {
                return new TextItem(item, ord);
            }
            case 2: {
                return new ArmourHud(item, ord);
            }
            case 3: {
                return new PotionEffects(item, ord);
            }
            case 4: {
                return new FPS(item, ord);
            }
            case 5: {
                return new PingDisplay(item, ord);
            }
            case 6: {
                return new DirectionHUD(item, ord);
            }
            case 7: {
                return new CpsDisplay(item, ord);
            }
            case 8: {
                return new ArrowCount(item, ord);
            }
            case 9: {
                return new CCounter(item, ord);
            }
            case 10: {
                return new TimeHud(item, ord);
            }
            default: {
                return null;
            }
        }
    }
    
    @Override
    public Map<String, String> getNames() {
        return this.names;
    }
    
    @Override
    public ChromaHUDDescription description() {
        return new ChromaHUDDescription("DEFAULT", "3.0", "ChromaHUD", "Default display items for ChromaHUD.");
    }
}
