package club.sk1er.mods.chromahud.gui;

import club.sk1er.mods.chromahud.*;
import club.sk1er.mods.chromahud.api.ButtonConfig;
import club.sk1er.mods.chromahud.api.DisplayItem;
import club.sk1er.mods.chromahud.api.StringConfig;
import club.sk1er.mods.chromahud.api.TextConfig;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;

public class EditItemsGui extends GuiScreen
{
    private DisplayElement element;
    private HashMap<GuiButton, Consumer<GuiButton>> clicks;
    private HashMap<GuiButton, Consumer<GuiButton>> updates;
    private HashMap<String, GuiButton> nameMap;
    private DisplayItem modifying;
    private int tmpId;
    private ChromaHUD mod;
    private boolean mouseLock;
    
    public EditItemsGui(final DisplayElement element, final ChromaHUD mod) {
        this.clicks = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.updates = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.nameMap = new HashMap<String, GuiButton>();
        this.element = element;
        this.mod = mod;
        this.mouseLock = Mouse.isButtonDown(0);
    }
    
    private int nextId() {
        return ++this.tmpId;
    }
    
    public void initGui() {
        this.reg("add", new GuiButton(this.nextId(), 2, 2, 100, 20, "Add Items"), guiButton -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new AddItemsGui(this.mod, this.element)), guiButton -> {});
        this.reg("Remove", new GuiButton(this.nextId(), 2, 23, 100, 20, "Remove Item"), guiButton -> {
            if (this.modifying != null) {
                this.element.removeDisplayItem(this.modifying.getOrdinal());
                if (this.modifying.getOrdinal() >= this.element.getDisplayItems().size()) {
                    this.modifying = null;
                }
            }
            return;
        }, guiButton -> guiButton.enabled = (this.modifying != null));
        this.reg("Move Up", new GuiButton(this.nextId(), 2, 44, 100, 20, "Move Up"), guiButton -> {
            if (this.modifying != null) {
                int i;
                i = this.modifying.getOrdinal();
                Collections.swap(this.element.getDisplayItems(), this.modifying.getOrdinal(), this.modifying.getOrdinal() - 1);
                this.element.adjustOrdinal();
            }
            return;
        }, guiButton -> guiButton.enabled = (this.modifying != null && this.modifying.getOrdinal() > 0));
        this.reg("Move Down", new GuiButton(this.nextId(), 2, 65, 100, 20, "Move Down"), guiButton -> {
            if (this.modifying != null) {
                int j;
                j = this.modifying.getOrdinal();
                Collections.swap(this.element.getDisplayItems(), this.modifying.getOrdinal(), this.modifying.getOrdinal() + 1);
                this.element.adjustOrdinal();
            }
            return;
        }, guiButton -> guiButton.enabled = (this.modifying != null && this.modifying.getOrdinal() < this.element.getDisplayItems().size() - 1));
        this.reg("Back", new GuiButton(this.nextId(), 2, ResolutionUtil.current().getScaledHeight() - 22, 100, 20, "Back"), guiButton -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new DisplayElementConfig(this.element, this.mod)), guiButton -> {});
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.clicks.get(button);
        if (guiButtonConsumer != null) {
            guiButtonConsumer.accept(button);
        }
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer) {
        this.reg(name, button, consumer, button1 -> {});
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer, final Consumer<GuiButton> tick) {
        this.buttonList.add(button);
        this.clicks.put(button, consumer);
        this.updates.put(button, tick);
        this.nameMap.put(name, button);
    }
    
    public void updateScreen() {
        super.updateScreen();
        for (final GuiButton guiButton : this.buttonList) {
            final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.updates.get(guiButton);
            if (guiButtonConsumer != null) {
                guiButtonConsumer.accept(guiButton);
            }
        }
    }
    
    public void onGuiClosed() {
        this.mod.saveState();
    }
    
    protected void keyTyped(final char typedChar, final int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        if (this.modifying == null) {
            return;
        }
        final List<TextConfig> textConfigs = ChromaHUDApi.getInstance().getTextConfigs(this.modifying.getType());
        if (textConfigs != null && !textConfigs.isEmpty()) {
            for (final TextConfig config : textConfigs) {
                final GuiTextField textField = config.getTextField();
                textField.textboxKeyTyped(typedChar, keyCode);
            }
        }
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        if (this.modifying != null) {
            final List<ButtonConfig> configs = ChromaHUDApi.getInstance().getButtonConfigs(this.modifying.getType());
            if (configs != null && !configs.isEmpty()) {
                for (final ButtonConfig config2 : configs) {
                    final GuiButton button2 = config2.getButton();
                    if (button2.mousePressed(this.mc, mouseX, mouseY)) {
                        config2.getAction().accept(button2, this.modifying);
                        return;
                    }
                }
            }
            final List<TextConfig> textConfigs = ChromaHUDApi.getInstance().getTextConfigs(this.modifying.getType());
            if (textConfigs != null && !textConfigs.isEmpty()) {
                for (final TextConfig config3 : textConfigs) {
                    final GuiTextField textField = config3.getTextField();
                    textField.mouseClicked(mouseX, mouseY, mouseButton);
                    if (textField.isFocused()) {
                        return;
                    }
                }
            }
        }
        super.mouseClicked(mouseX, mouseY, mouseButton);
        if (mouseButton == 0) {
            DisplayItem item1 = null;
            final ScaledResolution current = ResolutionUtil.current();
            final int xCenter = current.getScaledWidth() / 2;
            System.out.println(mouseX);
            System.out.println(mouseY);
            if (mouseX >= xCenter - 80 && mouseX <= xCenter + 80) {
                int yPosition = 40;
                for (final DisplayItem displayItem : this.element.getDisplayItems()) {
                    if (mouseY >= yPosition && mouseY <= yPosition + 20) {
                        item1 = displayItem;
                        break;
                    }
                    yPosition += 23;
                }
                System.out.println("adjusting mod");
            }
            for (final GuiButton guiButton : super.buttonList) {
                if (guiButton.isMouseOver()) {
                    return;
                }
            }
            this.modifying = item1;
            if (this.modifying != null) {
                ChromaHUDApi.getInstance().getTextConfigs(this.modifying.getType()).forEach(config -> config.getLoad().accept(config.getTextField(), this.modifying));
                ChromaHUDApi.getInstance().getButtonConfigs(this.modifying.getType()).forEach(button -> button.getLoad().accept(button.getButton(), this.modifying));
                ChromaHUDApi.getInstance().getStringConfigs(this.modifying.getType()).forEach(button -> button.getLoad().accept(this.modifying));
            }
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int state) {
        super.mouseReleased(mouseX, mouseY, state);
        if (this.modifying != null) {
            final List<ButtonConfig> configs = ChromaHUDApi.getInstance().getButtonConfigs(this.modifying.getType());
            if (configs != null && !configs.isEmpty()) {
                for (final ButtonConfig config : configs) {
                    final GuiButton button = config.getButton();
                    button.mouseReleased(mouseX, mouseY);
                }
            }
        }
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        final ScaledResolution current = ResolutionUtil.current();
        drawRect(0, 0, current.getScaledWidth(), current.getScaledHeight(), new Color(0, 0, 0, 150).getRGB());
        super.drawScreen(mouseX, mouseY, partialTicks);
        ElementRenderer.startDrawing(this.element);
        this.element.renderEditView();
        ElementRenderer.endDrawing(this.element);
        int xPosition = ResolutionUtil.current().getScaledWidth() / 2 - 80;
        int yPosition = 40;
        final int width = 160;
        final int height = 20;
        final Color defaultColor = new Color(255, 255, 255, 100);
        final Color otherColor = new Color(255, 255, 255, 150);
        for (final DisplayItem displayItem : this.element.getDisplayItems()) {
            final FontRenderer fontrenderer = this.mc.fontRenderer;
            GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
            final boolean hovered = mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
            drawRect(xPosition, yPosition, xPosition + width, yPosition + height, ((this.modifying != null && this.modifying.getOrdinal() == displayItem.getOrdinal()) || hovered) ? otherColor.getRGB() : defaultColor.getRGB());
            final int j = Color.RED.getRGB();
            final String displayString = ChromaHUDApi.getInstance().getName(displayItem.getType());
            fontrenderer.drawString(displayString, (float)(xPosition + width / 2 - fontrenderer.getStringWidth(displayString) / 2), (float)(yPosition + (height - 8) / 2), j, false);
            yPosition += 23;
        }
        if (this.modifying != null) {
            final List<ButtonConfig> configs = ChromaHUDApi.getInstance().getButtonConfigs(this.modifying.getType());
            xPosition = 3;
            yPosition = 89;
            if (configs != null && !configs.isEmpty()) {
                for (final ButtonConfig config : configs) {
                    final GuiButton button = config.getButton();
                    button.x = xPosition;
                    button.y = yPosition;
                    button.drawButton(this.mc, mouseX, mouseY,this.mc.getRenderPartialTicks());
                    yPosition += 23;
                }
            }
            final List<TextConfig> textConfigs = ChromaHUDApi.getInstance().getTextConfigs(this.modifying.getType());
            if (textConfigs != null && !textConfigs.isEmpty()) {
                for (final TextConfig config2 : textConfigs) {
                    final GuiTextField textField = config2.getTextField();
                    textField.x = xPosition;
                    textField.y = yPosition;
                    textField.drawTextBox();
                    yPosition += 23;
                    config2.getAction().accept(textField, this.modifying);
                }
            }
            final int rightBound = (int)(ResolutionUtil.current().getScaledWidth_double() / 2.0 - 90.0);
            final List<StringConfig> stringConfigs = ChromaHUDApi.getInstance().getStringConfigs(this.modifying.getType());
            if (stringConfigs != null && !stringConfigs.isEmpty()) {
                for (final StringConfig config3 : stringConfigs) {
                    config3.getDraw().accept(this.modifying);
                    final String draw = config3.getString();
                    final List<String> lines = new ArrayList<String>();
                    final String[] split = draw.split(" ");
                    final FontRenderer fontRendererObj = Minecraft.getMinecraft().fontRenderer;
                    StringBuilder currentLine = new StringBuilder();
                    for (final String s : split) {
                        if (!s.contains("\n")) {
                            if (fontRendererObj.getStringWidth(" " + currentLine.toString()) + fontRendererObj.getStringWidth(s) + xPosition < rightBound - 10) {
                                currentLine.append(" ").append(s);
                            }
                            else {
                                lines.add(currentLine.toString());
                                currentLine = new StringBuilder();
                                currentLine.append(s);
                            }
                        }
                        else {
                            final String[] split2 = s.split("\n");
                            final Iterator<String> iterator = Arrays.<String>asList(split2).iterator();
                            while (iterator.hasNext()) {
                                currentLine.append(" ").append(iterator.next());
                                if (iterator.hasNext()) {
                                    lines.add(currentLine.toString());
                                    currentLine = new StringBuilder();
                                }
                            }
                        }
                    }
                    lines.add(currentLine.toString());
                    yPosition += 10;
                    for (final String string : lines) {
                        Minecraft.getMinecraft().fontRenderer.drawString(string, xPosition, yPosition, Color.RED.getRGB());
                        yPosition += 10;
                    }
                }
            }
        }
    }
}
