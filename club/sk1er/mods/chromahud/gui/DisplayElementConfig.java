package club.sk1er.mods.chromahud.gui;

import club.sk1er.mods.chromahud.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.config.GuiSlider;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.function.Consumer;

public class DisplayElementConfig extends GuiScreen
{
    private DisplayElement element;
    private HashMap<GuiButton, Consumer<GuiButton>> clicks;
    private HashMap<GuiButton, Consumer<GuiButton>> updates;
    private HashMap<String, GuiButton> nameMap;
    private int ids;
    private int lastX;
    private int lastY;
    private DynamicTexture texture;
    private DynamicTexture texture2;
    private int hue;
    private int saturation;
    private int brightness;
    private ChromaHUD mod;
    private int lastWidth;
    private int lastHeight;
    private boolean mouseLock;
    
    public DisplayElementConfig(final DisplayElement element, final ChromaHUD mod) {
        this.clicks = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.updates = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.nameMap = new HashMap<String, GuiButton>();
        this.hue = -1;
        this.saturation = -1;
        this.brightness = 5;
        this.lastWidth = 0;
        this.lastHeight = 0;
        if (element == null) {
            throw new NullPointerException("Display element is null!");
        }
        this.mod = mod;
        this.element = element;
        this.regenImage();
        this.mouseLock = Mouse.isButtonDown(0);
    }
    
    public void regenImage() {
        final int dim = 256;
        final BufferedImage image = new BufferedImage(dim, dim, 1);
        for (int x = 0; x < dim; ++x) {
            for (int y = 0; y < dim; ++y) {
                image.setRGB(x, y, Color.HSBtoRGB((float)x / 256.0f, 1.0f - y / 256.0f, 1.0f));
            }
        }
        this.texture = new DynamicTexture(image);
        if (this.hue != -1 && this.saturation != -1) {
            final BufferedImage image2 = new BufferedImage(1, dim, 1);
            for (int y = 0; y < dim; ++y) {
                final float hue = this.hue / 256.0f;
                final float saturation = this.saturation / 256.0f;
                image2.setRGB(0, y, Color.HSBtoRGB(hue, saturation, 1.0f - y / 256.0f));
            }
            this.texture2 = new DynamicTexture(image2);
        }
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer) {
        this.reg(name, button, consumer, button1 -> {});
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer, final Consumer<GuiButton> tick) {
        this.buttonList.add(button);
        this.clicks.put(button, consumer);
        this.updates.put(button, tick);
        this.nameMap.put(name, button);
    }
    
    private int nextId() {
        return ++this.ids;
    }
    
    public void initGui() {
        super.initGui();
    }
    
    private void repack() {
        this.buttonList.clear();
        this.clicks.clear();
        this.updates.clear();
        this.nameMap.clear();
        final ScaledResolution current = ResolutionUtil.current();
        final int start_y = Math.max((int)(current.getScaledHeight_double() * 0.1) - 20, 5);
        final int posX = (int)(current.getScaledWidth_double() * 0.5) - 100;
        this.reg("pos", new GuiButton(this.nextId(), posX, start_y, "Change Position"), button -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new MoveElementGui(this.mod, this.element)));
        this.reg("items", new GuiButton(this.nextId(), posX, start_y + 22, "Change Items"), button -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new EditItemsGui(this.element, this.mod)));

        this.reg("Highlight", new GuiButton(this.nextId(), posX, start_y + 44, "-"), button -> this.element.setHighlighted(!this.element.isHighlighted()), button -> {
            String s;
            StringBuilder sb = new StringBuilder().append(TextFormatting.YELLOW.toString()).append("Highlighted: ");
            if (this.element.isHighlighted()) {
                s = TextFormatting.GREEN + "Yes";
            }
            else {
                s = TextFormatting.RED.toString() + "No";
            }
            button.displayString = sb.append(s).toString();
            return;
        });

        this.reg("shadow", new GuiButton(this.nextId(), posX, start_y + 66, "-"), button -> this.element.setShadow(!this.element.isShadow()), button -> {
            StringBuilder sb2 = new StringBuilder().append(TextFormatting.YELLOW.toString()).append("Shadow: ");
            String s2;
            if (this.element.isShadow()) {
                s2 = TextFormatting.GREEN + "Yes";
            }
            else {
                s2 = TextFormatting.RED.toString() + "No";
            }
            button.displayString = sb2.append(s2).toString();
            return;
        });
        this.reg("Scale Slider", (GuiButton)new GuiSlider(this.nextId(), posX, start_y + 88, 200, 20, "Scale: ", "", 50.0, 200.0, this.element.getScale() * 100.0, false, true), button -> {}, button -> {
            this.element.setScale(((GuiSlider)button).getValue() / 100.0);
            ((GuiButton)button).displayString = TextFormatting.YELLOW + "Scale: " + ((GuiSlider)button).getValueInt() + "%";
            System.out.println(this.element.getScale());
            return;
        });

        this.reg("color", new GuiButton(this.nextId(), posX, start_y + 110, "-"), button -> {
            if (this.element.isChroma()) {
                this.element.setChroma(false);
                this.element.setRgb(true);
            }
            else if (this.element.isRGB()) {
                this.element.setRgb(false);
                this.element.setColorPallet(true);
            }
            else {
                this.element.setColorPallet(false);
                this.element.setChroma(true);
            }
            return;
        }, button -> {
            String type;
            type = "Error";
            if (this.element.isRGB()) {
                type = "RGB";
            }
            if (this.element.isColorPallet()) {
                type = "Color Pallet";
            }
            if (this.element.isChroma()) {
                type = "Chroma";
            }
            button.displayString = TextFormatting.YELLOW + "Color mode: " + TextFormatting.GREEN.toString() + type;
            return;
        });

        this.reg("chromaMode", new GuiButton(this.nextId(), posX, start_y + 132, "-"), button -> this.element.setStaticChroma(!this.element.isStaticChroma()), button -> {
            String s3;
            StringBuilder sb3;
            if (!this.element.isChroma()) {
                button.enabled = false;
                button.visible = false;
            }
            else {
                button.visible = true;
                button.enabled = true;
                sb3 = new StringBuilder().append(TextFormatting.YELLOW).append("Chroma mode: ");
                if (this.element.isStaticChroma()) {
                    s3 = TextFormatting.GREEN + "Static";
                }
                else {
                    s3 = TextFormatting.GREEN + "Wave";
                }
                button.displayString = sb3.append(s3).toString();
            }
            return;
        });
        this.reg("redSlider", (GuiButton)new GuiSlider(this.nextId(), posX, start_y + 132, 200, 20, "Red: ", "", 0.0, 255.0, (double)this.element.getData().optInt("red"), false, true), button -> {}, button -> {
            if (!this.element.isRGB()) {
                ((GuiButton)button).enabled = false;
                ((GuiButton)button).visible = false;
            }
            else {
                ((GuiButton)button).visible = true;
                ((GuiButton)button).enabled = true;
                this.element.getData().put("red", ((GuiSlider)button).getValueInt());
                ((GuiButton)button).displayString = TextFormatting.YELLOW + "Red: " + this.element.getData().optInt("red");
            }
            return;
        });
        this.reg("blueSlider", (GuiButton)new GuiSlider(this.nextId(), posX, start_y + 176, 200, 20, "Blue: ", "", 0.0, 255.0, (double)this.element.getData().optInt("blue"), false, true), button -> {}, button -> {
            if (!this.element.isRGB()) {
                ((GuiButton)button).enabled = false;
                ((GuiButton)button).visible = false;
            }
            else {
                ((GuiButton)button).visible = true;
                ((GuiButton)button).enabled = true;
                this.element.getData().put("blue", ((GuiSlider)button).getValueInt());
                ((GuiButton)button).displayString = TextFormatting.YELLOW + "Blue: " + this.element.getData().optInt("blue");
            }
            return;
        });
        this.reg("greenSlider", (GuiButton)new GuiSlider(this.nextId(), posX, start_y + 154, 200, 20, "Green: ", "", 0.0, 255.0, (double)this.element.getData().optInt("green"), false, true), button -> {}, button -> {
            if (!this.element.isRGB()) {
                ((GuiButton)button).enabled = false;
                ((GuiButton)button).visible = false;
            }
            else {
                ((GuiButton)button).visible = true;
                ((GuiButton)button).enabled = true;
                this.element.getData().put("green", ((GuiSlider)button).getValueInt());
                ((GuiButton)button).displayString = TextFormatting.YELLOW + "Green: " + this.element.getData().optInt("green");
            }
            return;
        });
        this.reg("Back", new GuiButton(this.nextId(), 2, ResolutionUtil.current().getScaledHeight() - 22, 100, 20, "Back"), guiButton -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GeneralConfigGui(this.mod)), guiButton -> {});
        this.reg("Delete", new GuiButton(this.nextId(), 2, ResolutionUtil.current().getScaledHeight() - 44, 100, 20, "Delete"), guiButton -> {
            Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GeneralConfigGui(this.mod));
            ChromaHUDApi.getInstance().getElements().remove(this.element);
            System.out.println("Removed");
        }, guiButton -> {});
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.clicks.get(button);
        if (guiButtonConsumer != null) {
            guiButtonConsumer.accept(button);
        }
    }
    
    public void updateScreen() {
        final ScaledResolution current = ResolutionUtil.current();
        if (current.getScaledWidth() != this.lastWidth || current.getScaledHeight() != this.lastHeight) {
            this.repack();
            this.lastWidth = current.getScaledWidth();
            this.lastHeight = current.getScaledHeight();
        }
        if (this.element.isRGB()) {
            this.element.recalculateColor();
        }
        for (final GuiButton guiButton : this.buttonList) {
            final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.updates.get(guiButton);
            if (guiButtonConsumer != null) {
                guiButtonConsumer.accept(guiButton);
            }
        }
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.apply(mouseX, mouseY);
    }
    
    public float scale() {
        return this.availableSpace() / 285.0f;
    }
    
    private void apply(final int mouseX, final int mouseY) {
        if (this.mouseLock) {
            return;
        }
        if (!Mouse.isButtonDown(0)) {
            return;
        }
        if (!this.element.isColorPallet()) {
            return;
        }
        final ScaledResolution current = ResolutionUtil.current();
        final float scale = this.scale();
        final int left = this.posX(1);
        final int right = this.posX(2);
        final int top = this.posY(1);
        final int bottom = this.posY(3);
        float x = 0.0f;
        float y = 0.0f;
        if (mouseX > left && mouseX < right && mouseY > top && mouseY < bottom) {
            x = mouseX - left;
            y = mouseY - top;
            x /= scale;
            y /= scale;
            if (y > 0.0f && y <= 256.0f) {
                if (x < 256.0f && x > 0.0f) {
                    this.hue = (int)x;
                    this.saturation = (int)(256.0f - y);
                    System.out.println("Update: " + this.hue + " sat: " + this.saturation);
                    this.regenImage();
                    this.lastX = mouseX;
                    this.lastY = mouseY;
                }
                else if (x > 271.0f && x < 286.0f) {
                    System.out.println(y);
                    this.brightness = (int)y;
                    this.regenImage();
                }
            }
        }
    }
    
    public void onGuiClosed() {
        super.onGuiClosed();
        this.mod.saveState();
    }
    
    private void drawCircle(final int x, final int y) {
        GL11.glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
        Gui.drawRect(x - 2, y + 12, x + 2, y + 3, Color.BLACK.getRGB());
        Gui.drawRect(x - 2, y - 3, x + 2, y - 12, Color.BLACK.getRGB());
        Gui.drawRect(x + 12, y - 2, x + 3, y + 2, Color.BLACK.getRGB());
        Gui.drawRect(x - 12, y - 2, x - 3, y + 2, Color.BLACK.getRGB());
        Gui.drawRect(this.posX(2) + 5, (int)(this.startY() + (this.brightness - 2) * this.scale()), this.posX(2) + 15, (int)(this.startY() + (this.brightness + 2) * this.scale()), Color.BLACK.getRGB());
        this.element.setColor(Color.HSBtoRGB((float)this.hue / 255.0f, (float)this.saturation / 255.0f, 1.0f - this.brightness / 255.0f));
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        final ScaledResolution current = ResolutionUtil.current();
        this.mouseLock = (this.mouseLock && Mouse.isButtonDown(0));
        drawRect(0, 0, current.getScaledWidth(), current.getScaledHeight(), new Color(0, 0, 0, 150).getRGB());
        super.drawScreen(mouseX, mouseY, partialTicks);
        ElementRenderer.startDrawing(this.element);
        this.element.renderEditView();
        ElementRenderer.endDrawing(this.element);
        final int left = this.posX(1);
        final int top = this.posY(2);
        final int right = this.posX(2);
        final int size = right - left;
        final int bottom = this.posY(3);
        if (this.element.isRGB()) {
            final int start_y = Math.max((int)(current.getScaledHeight_double() * 0.1) - 20, 5) + 176 + 15;
            final int left2 = current.getScaledWidth() / 2 - 100;
            final int right2 = current.getScaledWidth() / 2 + 100;
            Gui.drawRect(left2, start_y, right2, right2 - left2 + 200, this.element.getColor());
        }
        if (!this.element.isColorPallet()) {
            return;
        }
        this.apply(mouseX, mouseY);
        GlStateManager.bindTexture(this.texture.getGlTextureId());
        GlStateManager.enableTexture2D();
        GL11.glPushMatrix();
        GL11.glTranslatef((float)left, (float)top, 0.0f);
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        GlStateManager.scale((float)size / 285.0f, (float)size / 285.0f, 0.0f);
        this.drawTexturedModalRect(0, 0, 0, 0, 256, 256);
        if (this.texture2 != null) {
            GlStateManager.bindTexture(this.texture2.getGlTextureId());
            GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            GL11.glTranslatef(271.0f, 0.0f, 0.0f);
            this.drawTexturedModalRect(0, 0, 0, 0, 15, 256);
        }
        GlStateManager.scale(285.0f / (float)size, 285.0f / (float)size, 0.0f);
        GL11.glPopMatrix();
        if (this.lastX != 0 && this.lastY != 0) {
            this.drawCircle(this.lastX, this.lastY);
        }
    }
    
    public int availableSpace() {
        final ScaledResolution current = ResolutionUtil.current();
        final int yMin = current.getScaledHeight() - 15 - this.startY();
        if (yMin + 20 > current.getScaledWidth()) {
            return yMin - 50;
        }
        return yMin;
    }
    
    private int startY() {
        final ScaledResolution current = ResolutionUtil.current();
        return (int)(Math.max(current.getScaledHeight_double() * 0.1 - 20.0, 5.0) + 132.0);
    }
    
    private int posX(final int vertex) {
        final int i = this.availableSpace();
        final ScaledResolution current = ResolutionUtil.current();
        switch (vertex) {
            case 1:
            case 3: {
                return (current.getScaledWidth() - i + 30) / 2;
            }
            case 2:
            case 4: {
                return (current.getScaledWidth() + i + 30) / 2;
            }
            default: {
                throw new IllegalArgumentException("Vertex not found " + vertex);
            }
        }
    }
    
    private int posY(final int vertex) {
        switch (vertex) {
            case 1:
            case 2: {
                return this.startY();
            }
            case 3:
            case 4: {
                return this.startY() + this.availableSpace();
            }
            default: {
                throw new IllegalArgumentException("Vertex not found " + vertex);
            }
        }
    }
}
