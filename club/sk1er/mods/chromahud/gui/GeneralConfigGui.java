package club.sk1er.mods.chromahud.gui;

import club.sk1er.mods.chromahud.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

public class GeneralConfigGui extends GuiScreen
{
    private ChromaHUD mod;
    private boolean mouseDown;
    private DisplayElement currentElement;
    private GuiButton edit;
    private HashMap<GuiButton, Consumer<GuiButton>> clicks;
    private boolean mouseLock;
    
    public GeneralConfigGui(final ChromaHUD mod) {
        this.clicks = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.mod = mod;
        this.mouseLock = Mouse.isButtonDown(0);
    }
    
    private void reg(final GuiButton button, final Consumer<GuiButton> consumer) {
        this.buttonList.add(button);
        this.clicks.put(button, consumer);
    }
    
    public void initGui() {
        super.initGui();
        this.reg(this.edit = new GuiButton(1, 5, 5, 100, 20, "Edit"), button -> {
            if (this.currentElement != null) {
                Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new DisplayElementConfig(this.currentElement, this.mod));
            }
            return;
        });
        this.reg(new GuiButton(2, 2, ResolutionUtil.current().getScaledHeight() - 22, 100, 20, "New"), guiButton -> {
            DisplayElement blank;
            blank = DisplayElement.blank();
            ChromaHUDApi.getInstance().getElements().add(blank);
            System.out.println(ChromaHUDApi.getInstance().getElements());
            Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new DisplayElementConfig(blank, this.mod));
            return;
        });
        this.edit.visible = false;
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int clickedMouseButton, final long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        final ScaledResolution current = ResolutionUtil.current();
        drawRect(0, 0, current.getScaledWidth(), current.getScaledHeight(), new Color(0, 0, 0, 150).getRGB());
        super.drawScreen(mouseX, mouseY, partialTicks);
        final List<DisplayElement> elementList = this.mod.getDisplayElements();
        for (final DisplayElement element : elementList) {
            ElementRenderer.startDrawing(element);
            try {
                element.drawForConfig();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            ElementRenderer.endDrawing(element);
        }
        if (this.currentElement != null) {
            final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
            final double x1 = this.currentElement.getXloc() * resolution.getScaledWidth_double();
            final double x2 = this.currentElement.getXloc() * resolution.getScaledWidth_double() + this.currentElement.getDimensions().getWidth();
            final double y1 = this.currentElement.getYloc() * resolution.getScaledHeight_double();
            final double y2 = this.currentElement.getYloc() * resolution.getScaledHeight_double() + this.currentElement.getDimensions().getHeight();
            this.drawHorizontalLine((int)(x1 - 5.0), (int)(x2 + 5.0), (int)y1 - 5, Color.RED.getRGB());
            this.drawHorizontalLine((int)(x1 - 5.0), (int)(x2 + 5.0), (int)y2 + 5, Color.RED.getRGB());
            this.drawVerticalLine((int)x1 - 5, (int)(y1 - 5.0), (int)(y2 + 5.0), Color.RED.getRGB());
            this.drawVerticalLine((int)x2 + 5, (int)(y1 - 5.0), (int)(y2 + 5.0), Color.RED.getRGB());
            this.edit.visible = true;
            int propX = (int)x1 - 5;
            int propY = (int)y1 - 30;
            if (propX < 10 || propX > resolution.getScaledWidth() - 200) {
                propX = resolution.getScaledWidth() / 2;
            }
            if (propY > resolution.getScaledHeight() - 5 || propY < 0) {
                propY = resolution.getScaledHeight() / 2;
            }
            this.edit.x = propX;
            this.edit.y = propY;
        }
        else {
            this.edit.visible = false;
        }
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }
    
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        final ScaledResolution current = ResolutionUtil.current();
        final int i = Mouse.getEventDWheel();
        final List<DisplayElement> elements = ChromaHUDApi.getInstance().getElements();
        if (elements.size() > 0) {
            if (i < 0) {
                if (this.currentElement == null) {
                    this.currentElement = elements.get(0);
                }
                else {
                    int i2 = elements.indexOf(this.currentElement);
                    if (++i2 > elements.size() - 1) {
                        i2 = 0;
                    }
                    this.currentElement = elements.get(i2);
                }
            }
            else if (i > 0) {
                if (this.currentElement == null) {
                    this.currentElement = elements.get(0);
                }
                else {
                    int i2 = elements.indexOf(this.currentElement);
                    if (--i2 < 0) {
                        i2 = elements.size() - 1;
                    }
                    this.currentElement = elements.get(i2);
                }
            }
        }
        boolean isOver = false;
        for (final GuiButton button : this.buttonList) {
            if (button.isMouseOver()) {
                isOver = true;
            }
        }
        if (!this.mouseDown && Mouse.isButtonDown(0) && !isOver) {
            final int clickX = Mouse.getX() / current.getScaleFactor();
            final int clickY = Mouse.getY() / current.getScaleFactor();
            boolean found = false;
            for (final DisplayElement element : this.mod.getDisplayElements()) {
                final Dimension dimension = element.getDimensions();
                final double displayXLoc = current.getScaledWidth_double() * element.getXloc();
                final double displayYLoc = current.getScaledHeight_double() - current.getScaledHeight_double() * element.getYloc();
                if (clickX > displayXLoc && clickX < displayXLoc + dimension.getWidth() && clickY < displayYLoc && clickY > displayYLoc - dimension.getHeight()) {
                    this.currentElement = element;
                    found = true;
                    break;
                }
            }
            if (!found) {
                this.currentElement = null;
            }
        }
        this.mouseDown = Mouse.isButtonDown(0);
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.clicks.get(button);
        if (guiButtonConsumer != null) {
            guiButtonConsumer.accept(button);
        }
    }
    
    public void onGuiClosed() {
        super.onGuiClosed();
        this.mod.saveState();
    }
}
