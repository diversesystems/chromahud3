package club.sk1er.mods.chromahud.gui;

import club.sk1er.mods.chromahud.ChromaHUD;
import club.sk1er.mods.chromahud.DisplayElement;
import club.sk1er.mods.chromahud.ElementRenderer;
import club.sk1er.mods.chromahud.ResolutionUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.function.Consumer;

public class MoveElementGui extends GuiScreen
{
    private HashMap<GuiButton, Consumer<GuiButton>> clicks;
    private HashMap<GuiButton, Consumer<GuiButton>> updates;
    private HashMap<String, GuiButton> nameMap;
    private ChromaHUD mod;
    private DisplayElement element;
    private GuiButton edit;
    private double lastX;
    private double lastY;
    private boolean lastD;
    private boolean mouseLock;
    
    public MoveElementGui(final ChromaHUD mod, final DisplayElement element) {
        this.clicks = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.updates = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.nameMap = new HashMap<String, GuiButton>();
        this.lastD = false;
        this.mod = mod;
        this.element = element;
        this.mouseLock = Mouse.isButtonDown(0);
    }
    
    public void initGui() {
        super.initGui();
        this.reg("", this.edit = new GuiButton(1, 5, 5, 100, 20, "Save"), button -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new DisplayElementConfig(this.element, this.mod)), guiButton -> {});
    }
    
    public void onGuiClosed() {
        super.onGuiClosed();
        this.mod.saveState();
    }
    
    public void updateScreen() {
        super.updateScreen();
        for (final GuiButton guiButton : this.buttonList) {
            final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.updates.get(guiButton);
            if (guiButtonConsumer != null) {
                guiButtonConsumer.accept(guiButton);
            }
        }
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.clicks.get(button);
        if (guiButtonConsumer != null) {
            guiButtonConsumer.accept(button);
        }
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        final ScaledResolution current = ResolutionUtil.current();
        drawRect(0, 0, current.getScaledWidth(), current.getScaledHeight(), new Color(0, 0, 0, 150).getRGB());
        this.mouseLock = (this.mouseLock && Mouse.isButtonDown(0));
        super.drawScreen(mouseX, mouseY, partialTicks);
        ElementRenderer.startDrawing(this.element);
        this.element.drawForConfig();
        ElementRenderer.endDrawing(this.element);
        final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
        final double x1 = this.element.getXloc() * resolution.getScaledWidth_double();
        final double x2 = this.element.getXloc() * resolution.getScaledWidth_double() + this.element.getDimensions().getWidth();
        final double y1 = this.element.getYloc() * resolution.getScaledHeight_double();
        final double y2 = this.element.getYloc() * resolution.getScaledHeight_double() + this.element.getDimensions().getHeight();
        this.drawHorizontalLine((int)(x1 - 5.0), (int)(x2 + 5.0), (int)y1 - 5, Color.RED.getRGB());
        this.drawHorizontalLine((int)(x1 - 5.0), (int)(x2 + 5.0), (int)y2 + 5, Color.RED.getRGB());
        this.drawVerticalLine((int)x1 - 5, (int)(y1 - 5.0), (int)(y2 + 5.0), Color.RED.getRGB());
        this.drawVerticalLine((int)x2 + 5, (int)(y1 - 5.0), (int)(y2 + 5.0), Color.RED.getRGB());
        int propX = (int)x1 - 5;
        int propY = (int)y1 - 30;
        if (propX < 10 || propX > resolution.getScaledWidth() - 200) {
            propX = resolution.getScaledWidth() / 2;
        }
        if (propY > resolution.getScaledHeight() - 5 || propY < 0) {
            propY = resolution.getScaledHeight() / 2;
        }
        this.edit.x = propX;
        this.edit.y = propY;
        if (Mouse.isButtonDown(0) && !this.mouseLock) {
            if ((mouseX > x1 && mouseX < x2 && mouseY > y1 && mouseY < y2) || this.lastD) {
                final double x3 = Mouse.getX() / ResolutionUtil.current().getScaledWidth_double();
                final double y3 = Mouse.getY() / ResolutionUtil.current().getScaledHeight_double();
                this.element.setXloc(this.element.getXloc() - (this.lastX - x3) / ResolutionUtil.current().getScaleFactor());
                this.element.setYloc(this.element.getYloc() + (this.lastY - y3) / ResolutionUtil.current().getScaleFactor());
                if (this.element.getXloc() < 0.0) {
                    this.element.setXloc(0.0);
                }
                if (this.element.getYloc() < 0.0) {
                    this.element.setYloc(0.0);
                }
                if (this.element.getXloc() * resolution.getScaledWidth() + this.element.getDimensions().width > resolution.getScaledWidth()) {
                    this.element.setXloc((resolution.getScaledWidth_double() - this.element.getDimensions().width) / resolution.getScaledWidth_double());
                }
                if (this.element.getYloc() * resolution.getScaledHeight() + this.element.getDimensions().height > resolution.getScaledHeight()) {
                    this.element.setYloc((resolution.getScaledHeight_double() - this.element.getDimensions().height) / resolution.getScaledHeight_double());
                }
                this.lastD = true;
            }
            else {
                this.lastD = false;
            }
        }
        else {
            this.lastD = false;
        }
        this.lastX = Mouse.getX() / ResolutionUtil.current().getScaledWidth_double();
        this.lastY = Mouse.getY() / ResolutionUtil.current().getScaledHeight_double();
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer, final Consumer<GuiButton> tick) {
        this.buttonList.add(button);
        this.clicks.put(button, consumer);
        this.updates.put(button, tick);
        this.nameMap.put(name, button);
    }
}
