package club.sk1er.mods.chromahud.gui;

import club.sk1er.mods.chromahud.*;
import club.sk1er.mods.chromahud.api.ChromaHUDDescription;
import club.sk1er.mods.chromahud.api.ChromaHUDParser;
import club.sk1er.mods.chromahud.api.DisplayItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class AddItemsGui extends GuiScreen
{
    private ChromaHUD mod;
    private DisplayElement element;
    private HashMap<GuiButton, Consumer<GuiButton>> clicks;
    private HashMap<GuiButton, Consumer<GuiButton>> updates;
    private HashMap<String, GuiButton> nameMap;
    private int tmpId;
    private boolean adding;
    private int offset;
    private List<DisplayElement> all;
    private DisplayElement target;
    private boolean mouseLock;
    
    public AddItemsGui(final ChromaHUD mod, final DisplayElement element) {
        this.clicks = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.updates = new HashMap<GuiButton, Consumer<GuiButton>>();
        this.nameMap = new HashMap<String, GuiButton>();
        this.tmpId = 0;
        this.adding = true;
        this.offset = 0;
        this.all = new ArrayList<DisplayElement>();
        this.mod = mod;
        this.element = element;
        for (final ChromaHUDParser chromaHUDParser : ChromaHUDApi.getInstance().getParsers()) {
            for (final String s : chromaHUDParser.getNames().keySet()) {
                final DisplayItem item = chromaHUDParser.parse(s, 0, new JsonHolder().put("type", s));
                final DisplayElement blank = DisplayElement.blank();
                blank.getDisplayItems().add(item);
                blank.adjustOrdinal();
                this.all.add(blank);
            }
        }
        for (final DisplayElement displayElement : this.all) {
            displayElement.drawForConfig();
        }
        (this.target = DisplayElement.blank()).setColor(Color.GREEN.getRGB());
        this.mouseLock = Mouse.isButtonDown(0);
    }
    
    private int nextId() {
        return ++this.tmpId;
    }
    
    public void initGui() {
        super.initGui();
        this.reg("Add", new GuiButton(this.nextId(), 2, 2, 100, 20, "Add"), guiButton -> {
            this.adding = true;
            this.offset = 0;
            return;
        }, guiButton -> guiButton.enabled = !this.adding);
        this.reg("Explore", new GuiButton(this.nextId(), 2, 23, 100, 20, "Explore"), guiButton -> {
            this.adding = false;
            this.offset = 0;
            return;
        }, guiButton -> guiButton.enabled = this.adding);
        this.reg("Down", new GuiButton(this.nextId(), 2, 65, 100, 20, "Scroll Down"), guiButton -> this.offset += 50, guiButton -> {});
        this.reg("Up", new GuiButton(this.nextId(), 2, 44, 100, 20, "Scroll Up"), guiButton -> this.offset -= 50, guiButton -> {});
        this.reg("Back", new GuiButton(this.nextId(), 2, ResolutionUtil.current().getScaledHeight() - 22, 100, 20, "Back"), guiButton -> Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new EditItemsGui(this.element, this.mod)), guiButton -> {});
    }
    
    private void reg(final String name, final GuiButton button, final Consumer<GuiButton> consumer, final Consumer<GuiButton> tick) {
        this.buttonList.add(button);
        this.clicks.put(button, consumer);
        this.updates.put(button, tick);
        this.nameMap.put(name, button);
    }
    
    protected void actionPerformed(final GuiButton button) throws IOException {
        final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.clicks.get(button);
        if (guiButtonConsumer != null) {
            guiButtonConsumer.accept(button);
        }
    }
    
    public void updateScreen() {
        super.updateScreen();
        for (final GuiButton guiButton : this.buttonList) {
            final Consumer<GuiButton> guiButtonConsumer = (Consumer<GuiButton>)this.updates.get(guiButton);
            if (guiButtonConsumer != null) {
                guiButtonConsumer.accept(guiButton);
            }
        }
    }
    
    public void drawDefaultBackground() {
        super.drawDefaultBackground();
    }
    
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();
        final int i = Mouse.getEventDWheel();
        if (i < 0) {
            this.offset += 10;
        }
        else if (i > 0) {
            this.offset -= 10;
        }
    }
    
    public void onGuiClosed() {
        this.mod.saveState();
    }
    
    public void drawScreen(final int mouseX, final int mouseY, final float partialTicks) {
        this.mouseLock = (this.mouseLock && Mouse.isButtonDown(0));
        final ScaledResolution current = ResolutionUtil.current();
        drawRect(0, 0, current.getScaledWidth(), current.getScaledHeight(), new Color(0, 0, 0, 150).getRGB());
        super.drawScreen(mouseX, mouseY, partialTicks);
        ElementRenderer.startDrawing(this.target);
        if (this.adding) {
            final Color defaultColor = new Color(255, 255, 255, 100);
            int cursorY = 50 + this.offset;
            this.drawCenteredString(this.mc.fontRenderer, "Click Explore to see examples!", current.getScaledWidth() / 2, cursorY - 30, Color.RED.getRGB());
            final List<ChromaHUDParser> parsers = ChromaHUDApi.getInstance().getParsers();
            for (final ChromaHUDParser parser : parsers) {
                final Map<String, String> names = parser.getNames();
                for (final String s : names.keySet()) {
                    final String text1 = names.get(s) + "";
                    drawRect(current.getScaledWidth() / 2 - 80, cursorY, current.getScaledWidth() / 2 + 80, cursorY + 20, defaultColor.getRGB());
                    final int j = Color.RED.getRGB();
                    final int width = 160;
                    final int height = 20;
                    this.mc.fontRenderer.drawString(text1, (float)(current.getScaledWidth() / 2 - 80 + width / 2 - this.mc.fontRenderer.getStringWidth(text1) / 2), (float)(cursorY + (height - 8) / 2), j, false);
                    final int i = ResolutionUtil.current().getScaledHeight() - Mouse.getY() / current.getScaleFactor();
                    if (Mouse.isButtonDown(0) && !this.mouseLock && i >= cursorY && i <= cursorY + 23) {
                        final int i2 = Mouse.getX() / current.getScaleFactor();
                        if (i2 >= current.getScaledWidth() / 2 - 80 && i2 <= current.getScaledWidth() / 2 + 80) {
                            final DisplayItem item = ChromaHUDApi.getInstance().parse(s, 0, new JsonHolder().put("type", s));
                            this.element.getDisplayItems().add(item);
                            this.element.adjustOrdinal();
                            Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new EditItemsGui(this.element, this.mod));
                        }
                    }
                    cursorY += 23;
                }
            }
        }
        else {
            int cursorY2 = 50 + this.offset;
            final List<ChromaHUDParser> parsers2 = ChromaHUDApi.getInstance().getParsers();
            for (final ChromaHUDParser parser2 : parsers2) {
                final ChromaHUDDescription description = parser2.description();
                final String text2 = "Items in " + description.getName() + ".";
                this.mc.fontRenderer.drawString(text2, (float)((current.getScaledWidth() - this.mc.fontRenderer.getStringWidth(text2)) / 2), (float)cursorY2, Color.RED.getRGB(), true);
                cursorY2 += 30;
                final Map<String, String> names2 = parser2.getNames();
                for (final String s2 : names2.keySet()) {
                    final String text3 = names2.get(s2) + ": ";
                    final DisplayElement displayElement = this.find(s2);
                    if (displayElement == null) {
                        final String text4 = "ERROR LOCATING DISPLAY ELEMENT " + s2;
                        this.mc.fontRenderer.drawString(text4, (float)((current.getScaledWidth() - this.mc.fontRenderer.getStringWidth(text4)) / 2), (float)cursorY2, Color.RED.getRGB(), true);
                        cursorY2 += 15;
                    }
                    else {
                        final Dimension dimensions = displayElement.getDimensions();
                        final int stringWidth = this.mc.fontRenderer.getStringWidth(text3);
                        final double totalWidth = dimensions.getWidth() + stringWidth;
                        final double left = (current.getScaledWidth_double() - totalWidth) / 2.0;
                        final double startDraw = left + stringWidth;
                        displayElement.setXloc(startDraw / current.getScaledWidth_double());
                        displayElement.setYloc((double)cursorY2 / current.getScaledHeight_double());
                        displayElement.drawForConfig();
                        this.mc.fontRenderer.drawString(text3, (float)((current.getScaledWidth() - totalWidth) / 2.0), (float)cursorY2, Color.RED.getRGB(), true);
                        cursorY2 += dimensions.height + 5;
                    }
                }
            }
        }
        ElementRenderer.endDrawing(this.target);
    }
    
    private DisplayElement find(final String key) {
        for (final DisplayElement displayElement : this.all) {
            if (((DisplayItem)displayElement.getDisplayItems().get(0)).getType().equalsIgnoreCase(key)) {
                return displayElement;
            }
        }
        return null;
    }
}
