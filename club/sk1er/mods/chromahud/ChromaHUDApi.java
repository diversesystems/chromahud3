package club.sk1er.mods.chromahud;

import club.sk1er.mods.chromahud.api.*;
import club.sk1er.mods.chromahud.displayitems.TextItem;
import com.google.gson.JsonArray;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class ChromaHUDApi
{
    public static final String VERSION = "3.0";
    private static ChromaHUDApi instance;
    private List<ChromaHUDParser> parsers;
    private HashMap<String, String> names;
    private List<DisplayElement> elements;
    private boolean posted;
    private HashMap<String, ArrayList<ButtonConfig>> buttonConfigs;
    private HashMap<String, ArrayList<TextConfig>> textConfigs;
    private HashMap<String, ArrayList<StringConfig>> stringConfigs;
    
    private ChromaHUDApi() {
        this.parsers = new ArrayList<ChromaHUDParser>();
        this.names = new HashMap<String, String>();
        this.elements = new ArrayList<DisplayElement>();
        this.posted = false;
        this.buttonConfigs = new HashMap<String, ArrayList<ButtonConfig>>();
        this.textConfigs = new HashMap<String, ArrayList<TextConfig>>();
        this.stringConfigs = new HashMap<String, ArrayList<StringConfig>>();
        ChromaHUDApi.instance = this;
    }
    
    public static ChromaHUDApi getInstance() {
        if (ChromaHUDApi.instance == null) {
            ChromaHUDApi.instance = new ChromaHUDApi();
        }
        return ChromaHUDApi.instance;
    }
    
    public List<ButtonConfig> getButtonConfigs(String type) {
        type = type.toLowerCase();
        final ArrayList<ButtonConfig> configs = (ArrayList<ButtonConfig>)this.buttonConfigs.get(type);
        if (configs != null) {
            return new ArrayList<ButtonConfig>(configs);
        }
        return new ArrayList<ButtonConfig>();
    }
    
    public List<TextConfig> getTextConfigs(String type) {
        type = type.toLowerCase();
        final ArrayList<TextConfig> configs = (ArrayList<TextConfig>)this.textConfigs.get(type);
        if (configs != null) {
            return new ArrayList<TextConfig>(configs);
        }
        return new ArrayList<TextConfig>();
    }
    
    public List<StringConfig> getStringConfigs(String type) {
        type = type.toLowerCase();
        final ArrayList<StringConfig> configs = (ArrayList<StringConfig>)this.stringConfigs.get(type);
        if (configs != null) {
            return new ArrayList<StringConfig>(configs);
        }
        return new ArrayList<StringConfig>();
    }
    
    public List<DisplayElement> getElements() {
        return this.elements;
    }
    
    public void registerTextConfig(String type, final TextConfig config) {
        type = type.toLowerCase();
        if (!this.textConfigs.containsKey(type)) {
            this.textConfigs.put(type, new ArrayList<TextConfig>());
        }
        ((ArrayList<TextConfig>)this.textConfigs.get(type)).add(config);
    }
    
    public void registerStringConfig(String type, final StringConfig config) {
        type = type.toLowerCase();
        if (!this.stringConfigs.containsKey(type)) {
            this.stringConfigs.put(type, new ArrayList<StringConfig>());
        }
        ((ArrayList<StringConfig>)this.stringConfigs.get(type)).add(config);
    }
    
    public void registerButtonConfig(String type, final ButtonConfig config) {
        type = type.toLowerCase();
        if (!this.buttonConfigs.containsKey(type)) {
            this.buttonConfigs.put(type, new ArrayList<ButtonConfig>());
        }
        ((ArrayList<ButtonConfig>)this.buttonConfigs.get(type)).add(config);
    }
    
    public void register(final ChromaHUDParser parser) {
        if (this.posted) {
            throw new IllegalStateException("Cannot register parser after FMLPostInitialization event");
        }
        this.parsers.add(parser);
        this.names.putAll(parser.getNames());
    }
    
    protected void post(final JsonHolder config) {
        if (this.posted) {
            throw new IllegalStateException("Already posted!");
        }
        this.posted = true;
        this.elements.clear();
        final JsonArray displayElements = config.optJSONArray("elements");
        for (int i = 0; i < displayElements.size(); ++i) {
            final JsonHolder object = new JsonHolder(displayElements.get(i).getAsJsonObject());
            try {
                final DisplayElement e = new DisplayElement(object);
                if (e.getDisplayItems().size() > 0) {
                    this.elements.add(e);
                }
            }
            catch (Exception e2) {
                e2.printStackTrace();
                Logger.getLogger("ChromaHUD").severe("A fatal error occurred while loading the display element " + object);
            }
        }
        if (!config.has("elements")) {
            final DisplayElement blank = DisplayElement.blank();
            final TextItem e3 = new TextItem(new JsonHolder().put("type", "TEXT"), 0);
            e3.setText("/chromahud to get started :)");
            blank.setHighlighted(true);
            blank.setColor(Color.RED.getRGB());
            blank.getDisplayItems().add(e3);
            this.getElements().add(blank);
        }
    }
    
    public DisplayItem parse(final String type, final int ord, final JsonHolder item) {
        for (final ChromaHUDParser parser : this.parsers) {
            final DisplayItem parsed = parser.parse(type, ord, item);
            if (parsed != null) {
                return parsed;
            }
        }
        return null;
    }
    
    public String getName(final String type) {
        return this.names.getOrDefault(type, type);
    }
    
    public List<ChromaHUDParser> getParsers() {
        return new ArrayList<ChromaHUDParser>(this.parsers);
    }
}
