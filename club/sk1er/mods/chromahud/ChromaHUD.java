package club.sk1er.mods.chromahud;

import club.sk1er.mods.chromahud.api.ButtonConfig;
import club.sk1er.mods.chromahud.api.DisplayItem;
import club.sk1er.mods.chromahud.api.StringConfig;
import club.sk1er.mods.chromahud.api.TextConfig;
import club.sk1er.mods.chromahud.commands.CommandChromaHud;
import club.sk1er.mods.chromahud.displayitems.ArmourHud;
import club.sk1er.mods.chromahud.displayitems.CordsDisplay;
import club.sk1er.mods.chromahud.displayitems.TextItem;
import club.sk1er.mods.chromahud.displayitems.TimeHud;
import club.sk1er.mods.chromahud.gui.GeneralConfigGui;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.command.ICommand;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.io.*;
import java.util.List;

@Mod(modid = "chromahud", version = "3.0")
public class ChromaHUD
{
    public static final String MODID = "ChromaHUD";
    public static final String VERSION = "3.0";
    private File suggestedConfigurationFile;
    private boolean enabled;
    
    public ChromaHUD() {
        this.enabled = true;
    }
    
    @Mod.EventHandler
    public void preInit(final FMLPreInitializationEvent event) {
        this.suggestedConfigurationFile = event.getSuggestedConfigurationFile();
        ChromaHUDApi.getInstance();
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        ChromaHUDApi.getInstance().register(new DefaultChromaHudParser());

        ChromaHUDApi.getInstance().registerButtonConfig("CORDS", new ButtonConfig((guiButton, displayItem) -> {
            CordsDisplay displayItem2 = (CordsDisplay) displayItem;
            displayItem2.state = ((displayItem2.state != 1) ? 1 : 0);
            guiButton.displayString = TextFormatting.RED.toString() + "Make " + ((((CordsDisplay)displayItem).state == 1) ? "Horizontal" : "Vertical");
            return;
        }, new GuiButton(0, 0, 0, "Cords State"), (guiButton, displayItem) -> guiButton.displayString = TextFormatting.RED.toString() + "Make " + ((((CordsDisplay)displayItem).state == 1) ? "Horizontal" : "Vertical")));

        ChromaHUDApi.getInstance().registerButtonConfig("CORDS", new ButtonConfig((guiButton, displayItem) -> {
            CordsDisplay cordsDisplay;
            CordsDisplay displayItem3;
            int next;
            int next2;
            displayItem3 = cordsDisplay = (CordsDisplay) displayItem;
            ++cordsDisplay.precision;
            if (displayItem3.precision > 4) {
                displayItem3.precision = 0;
            }
            next = displayItem3.precision + 1;
            if (next > 4) {
                next = 0;
            }
            guiButton.displayString = TextFormatting.RED.toString() + "Change to " + next + " decimal" + ((next != 1) ? "s" : "");
            return;
        }, new GuiButton(0, 0, 0, "Coords Precision"), (guiButton, displayItem) -> {
            int next2;
            next2 = ((CordsDisplay)displayItem).precision + 1;
            if (next2 > 4) {
                next2 = 0;
            }
            guiButton.displayString = TextFormatting.RED.toString() + "Change to " + next2 + " decimal" + ((next2 != 1) ? "s" : "");
            return;
        }));

        ChromaHUDApi.getInstance().registerButtonConfig("ARMOUR_HUD", new ButtonConfig((guiButton, displayItem) -> {
            ArmourHud item;
            item = (ArmourHud) displayItem;
            item.toggleDurability();
            guiButton.displayString = TextFormatting.RED.toString() + "Toggle Durability";
            return;
        }, new GuiButton(0, 0, 0, "Armour Hud Durability"), (guiButton, displayItem) -> guiButton.displayString = TextFormatting.RED.toString() + "Toggle Durability"));

        ChromaHUDApi.getInstance().registerButtonConfig("ARMOUR_HUD", new ButtonConfig((guiButton, displayItem) -> {
            final ArmourHud item2;
            item2 = (ArmourHud) displayItem;
            item2.toggleHand();
            guiButton.displayString = TextFormatting.RED.toString() + "Toggle Held Item";
            return;
        }, new GuiButton(0, 0, 0, "Armour Hud Hand"), (guiButton, displayItem) -> guiButton.displayString = TextFormatting.RED.toString() + "Toggle Held Item"));
        final GuiTextField textTextField = new GuiTextField(1, Minecraft.getMinecraft().fontRenderer, 0, 0, 200, 20);
        ChromaHUDApi.getInstance().registerTextConfig("TEXT", new TextConfig((guiTextField, displayItem) -> ((TextItem)displayItem).setText(guiTextField.getText()), textTextField, (guiTextField, displayItem) -> guiTextField.setText(((TextItem)displayItem).getText())));
        ChromaHUDApi.getInstance().registerTextConfig("TIME", new TextConfig((guiTextField, displayItem) -> ((TimeHud)displayItem).setFormat(guiTextField.getText()), textTextField, (guiTextField, displayItem) -> guiTextField.setText(((TimeHud)displayItem).getFormat())));
        ChromaHUDApi.getInstance().registerStringConfig("TIME", new StringConfig("Accepted Formats\nYY - Year\nMM - Month\ndd - Day\nHH - Hour\nmm - Minute\nss - Second\nFor more options, Google \"Date Format\""));
    }
    
    @Mod.EventHandler
    public void postInit(final FMLPostInitializationEvent event) {
        this.setup();
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandChromaHud(this));
        MinecraftForge.EVENT_BUS.register((Object)new ElementRenderer(this));
    }
    
    public void setup() {
        JsonHolder data = new JsonHolder();
        try {
            final FileReader fr = new FileReader(this.suggestedConfigurationFile);
            final BufferedReader br = new BufferedReader(fr);
            final StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
            final String done = builder.toString();
            data = new JsonHolder(done);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        ChromaHUDApi.getInstance().post(data);
    }
    
    public List<DisplayElement> getDisplayElements() {
        return ChromaHUDApi.getInstance().getElements();
    }
    
    public GeneralConfigGui getConfigGuiInstance() {
        return new GeneralConfigGui(this);
    }
    
    public void saveState() {
        final JsonHolder master = new JsonHolder();
        master.put("enabled", this.enabled);
        final JsonArray elementArray = new JsonArray();
        master.put("elements", elementArray);
        for (final DisplayElement element : this.getDisplayElements()) {
            final JsonHolder tmp = element.getData();
            final JsonArray items = new JsonArray();
            for (final DisplayItem item : element.getDisplayItems()) {
                final JsonHolder raw = item.getData();
                raw.put("type", item.getType());
                items.add((JsonElement)raw.getObject());
            }
            elementArray.add((JsonElement)tmp.getObject());
            tmp.put("items", items);
        }
        try {
            if (!this.suggestedConfigurationFile.exists()) {
                this.suggestedConfigurationFile.createNewFile();
            }
            final FileWriter fw = new FileWriter(this.suggestedConfigurationFile);
            final BufferedWriter bw = new BufferedWriter(fw);
            bw.write(master.toString());
            bw.close();
            fw.close();
        }
        catch (Exception ex) {}
    }
}
