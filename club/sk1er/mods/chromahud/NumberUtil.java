package club.sk1er.mods.chromahud;

public class NumberUtil
{
    public static double round(final double in, final double places) {
        if (places == 0.0) {
            return Math.round(in);
        }
        return Math.round(in * 10.0 * places) / (10.0 * places);
    }
}
