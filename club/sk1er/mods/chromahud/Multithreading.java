package club.sk1er.mods.chromahud;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Multithreading
{
    public static ExecutorService POOL;
    private static ScheduledExecutorService RUNNABLE_POOL;
    
    public static void schedule(final Runnable r, final long initialDelay, final long delay, final TimeUnit unit) {
        Multithreading.RUNNABLE_POOL.scheduleAtFixedRate(r, initialDelay, delay, unit);
    }
    
    public static void runAsync(final Runnable runnable) {
        Multithreading.POOL.execute(runnable);
    }
    
    public static int getTotal() {
        final ThreadPoolExecutor tpe = (ThreadPoolExecutor)Multithreading.POOL;
        return tpe.getActiveCount();
    }
    
    static {
        Multithreading.POOL = Executors.newFixedThreadPool(100, new ThreadFactory() {
            AtomicInteger counter = new AtomicInteger(0);
            
            @Override
            public Thread newThread(final Runnable r) {
                return new Thread(r, String.format("Thread %s", this.counter.incrementAndGet()));
            }
        });
        Multithreading.RUNNABLE_POOL = Executors.newScheduledThreadPool(3, new ThreadFactory() {
            private AtomicInteger counter = new AtomicInteger(0);
            
            @Override
            public Thread newThread(final Runnable r) {
                return new Thread(r, String.format("Thread " + this.counter.incrementAndGet(), new Object[0]));
            }
        });
    }
}
