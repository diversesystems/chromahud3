package club.sk1er.mods.chromahud;

public interface GenKeyCallback
{
    void call(final JsonHolder p0);
}
