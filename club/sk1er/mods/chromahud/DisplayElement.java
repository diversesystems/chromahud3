package club.sk1er.mods.chromahud;

import club.sk1er.mods.chromahud.api.Dimension;
import club.sk1er.mods.chromahud.api.DisplayItem;
import com.google.gson.JsonArray;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class DisplayElement
{
    private double xloc;
    private double yloc;
    private List<DisplayItem> displayItems;
    private double scale;
    private int color;
    private double prevX;
    private double prevY;
    private boolean shadow;
    private boolean highlighted;
    private JsonHolder data;
    private double brightness;
    
    public DisplayElement(final JsonHolder object) {
        this.displayItems = new ArrayList<DisplayItem>();
        this.scale = 1.0;
        this.data = object;
        this.xloc = object.optDouble("x");
        this.yloc = object.optDouble("y");
        this.scale = object.optDouble("scale");
        final List<DisplayItem> items = new ArrayList<DisplayItem>();
        final JsonArray itemss = object.optJSONArray("items");
        int ord = 0;
        for (int i1 = 0; i1 < itemss.size(); ++i1) {
            final JsonHolder item = new JsonHolder(itemss.get(i1).getAsJsonObject());
            final DisplayItem type = ChromaHUDApi.getInstance().parse(item.optString("type"), ord, item);
            if (type != null) {
                items.add(type);
                ++ord;
            }
        }
        this.displayItems = items;
        this.shadow = object.optBoolean("shadow");
        this.highlighted = object.optBoolean("highlighted");
        this.brightness = this.data.optDouble("brightness");
        this.color = this.data.optInt("color");
        this.recalculateColor();
    }
    
    public static DisplayElement blank() {
        return new DisplayElement(new JsonHolder().put("x", 0.5).put("y", 0.5).put("scale", 1).put("color", Color.WHITE.getRGB()).put("color_pallet", true));
    }
    
    public double getBrightness() {
        return this.data.optDouble("brightness");
    }
    
    public void setBrightness(final double brightness) {
        this.data.put("brightness", brightness);
    }
    
    @Override
    public String toString() {
        return "DisplayElement{xloc=" + this.xloc + ", yloc=" + this.yloc + ", displayItems=" + this.displayItems + ", scale=" + this.scale + ", color=" + this.color + '}';
    }
    
    public boolean isChroma() {
        return this.data.optBoolean("chroma");
    }
    
    public void setChroma(final boolean chroma) {
        this.data.put("chroma", chroma);
    }
    
    public void recalculateColor() {
        if (this.isChroma()) {
            this.color = 0;
        }
        else if (this.isRGB()) {
            this.color = new Color(this.data.optInt("red"), this.data.optInt("green"), this.data.optInt("blue")).getRGB();
        }
    }
    
    public void reformatColor() {
        if (this.isChroma()) {}
    }
    
    public void draw() {
        final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
        final int x = (int)(this.xloc * resolution.getScaledWidth_double());
        double y = (double)(int)(this.yloc * resolution.getScaledHeight_double());
        for (final DisplayItem iDisplayItem : this.displayItems) {
            try {
                final Dimension d = iDisplayItem.draw(x, y, false);
                y += d.getHeight() * ElementRenderer.getCurrentScale();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public int getColor() {
        return this.color;
    }
    
    public void setColor(final int color) {
        this.color = color;
        this.data.put("color", color);
    }
    
    public double getXloc() {
        return this.xloc;
    }
    
    public void setXloc(final double xloc) {
        this.data.put("x", xloc);
        this.xloc = xloc;
    }
    
    public void removeDisplayItem(final int ordinal) {
        this.displayItems.remove(ordinal);
        this.adjustOrdinal();
    }
    
    public double getYloc() {
        return this.yloc;
    }
    
    public void setYloc(final double yloc) {
        this.data.put("y", yloc);
        this.yloc = yloc;
    }
    
    public List<DisplayItem> getDisplayItems() {
        return this.displayItems;
    }
    
    public double getScale() {
        return this.scale;
    }
    
    public void setScale(final double scale) {
        this.data.put("scale", scale);
        this.scale = scale;
    }
    
    public java.awt.Dimension getDimensions() {
        return new java.awt.Dimension((int)this.prevX, (int)this.prevY);
    }
    
    public void drawForConfig() {
        this.recalculateColor();
        this.prevX = 0.0;
        this.prevY = 0.0;
        final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
        double addy = 0.0;
        final int x = (int)(this.xloc * resolution.getScaledWidth_double());
        double y = (double)(int)(this.yloc * resolution.getScaledHeight_double());
        for (final DisplayItem iDisplayItem : this.displayItems) {
            final Dimension d = iDisplayItem.draw(x, y, true);
            y += d.getHeight() * ElementRenderer.getCurrentScale();
            addy += d.getHeight() * ElementRenderer.getCurrentScale();
            this.prevX = (int)Math.max(d.getWidth() * ElementRenderer.getCurrentScale(), this.prevX);
        }
        this.prevY = addy;
    }
    
    public void renderEditView() {
        final ScaledResolution resolution = new ScaledResolution(Minecraft.getMinecraft());
        final int x = (int)(0.8 * resolution.getScaledWidth_double());
        double y = (double)(int)(0.2 * resolution.getScaledHeight_double());
        for (final DisplayItem iDisplayItem : this.displayItems) {
            final Dimension d = iDisplayItem.draw(x, y, false);
            y += d.getHeight() * ElementRenderer.getCurrentScale();
        }
    }
    
    public void adjustOrdinal() {
        for (int ord = 0; ord < this.displayItems.size(); ++ord) {
            ((DisplayItem)this.displayItems.get(ord)).setOrdinal(ord);
        }
    }
    
    public void setRgb(final boolean state) {
        this.data.put("rgb", state);
    }
    
    public boolean isRGB() {
        return this.data.optBoolean("rgb");
    }
    
    public boolean isColorPallet() {
        return this.data.optBoolean("color_pallet");
    }
    
    public void setColorPallet(final boolean state) {
        this.data.put("color_pallet", state);
    }
    
    public boolean isStaticChroma() {
        return this.data.optBoolean("static_chroma");
    }
    
    public void setStaticChroma(final boolean state) {
        this.data.put("static_chroma", state);
    }
    
    public boolean isShadow() {
        return this.shadow;
    }
    
    public void setShadow(final boolean shadow) {
        this.data.put("shadow", shadow);
        this.shadow = shadow;
    }
    
    public boolean isHighlighted() {
        return this.highlighted;
    }
    
    public void setHighlighted(final boolean highlighted) {
        this.data.put("highlighted", highlighted);
        this.highlighted = highlighted;
    }
    
    public JsonHolder getData() {
        return this.data;
    }
}
