package club.sk1er.mods.chromahud;

public class Sk1erException extends Exception
{
    public Sk1erException(final String exc) {
        super(exc);
    }
}
